package ch.fhnw.richards.exams.nochNieVerwendet;

import java.math.BigDecimal;
import java.util.ArrayList;

public class GenericsQuestion {

	// Data classes. In order to keep the code short,
	// we use package-private attributes, instead of
	// private attributes with getters and setters
	public static abstract class Product {
		long ProductID;
		String ProductName;
	}
	public static class Book extends Product {
		String AuthorName;
	}
	public static class CD extends Product {
		ArrayList<String> trackNames;
	}
	
	// Shopping cart
	public static class ShoppingCart<T extends Product> {
		Item<T> itemList;
		BigDecimal shippingCost;
		
		public void add(T product) {
			Item<T> newItem = new Item<T>(product);
			newItem.next = itemList;
			itemList = newItem;
		}
	}
	
	// Items in cart
	private static class Item<T extends Product> {
		T article;
		int quantity;
		BigDecimal preis;
		Item<T> next;
		
		private Item(T article) {
			this.article = article;
			quantity = 1;
			preis = BigDecimal.ZERO;
			next = null;
		}
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
