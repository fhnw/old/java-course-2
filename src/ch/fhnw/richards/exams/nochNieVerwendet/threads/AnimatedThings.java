package ch.fhnw.richards.exams.nochNieVerwendet.threads;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

public class AnimatedThings extends JFrame {
	private static final int NUM_THINGS = 3;
	private ArrayList<Thing> things = new ArrayList<Thing>();
	private Thread repaintThread;
	private ThingCanvas canvas;
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		AnimatedThings anim = new AnimatedThings();
		anim.startAnimation();
	}

	/**
	 * 
	 */
	private AnimatedThings() {
		// Set up the GUI: a single Canvas to draw on
		this.setLayout(new BorderLayout());
		canvas = new ThingCanvas();
		canvas.setPreferredSize(new Dimension(400,300));
		canvas.setDoubleBuffered(true);
		this.add(canvas, BorderLayout.CENTER);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.pack();
		this.setVisible(true);
	}
	
	private void startAnimation() {
		// Get the size of the canvas
		int width = canvas.getWidth();
		int height = canvas.getHeight();
		
		// Create some animated things
		things.add(new Thing(Color.RED, 25, width, height));
		things.add(new Thing(Color.GREEN, 25, width, height));
		things.add(new Thing(Color.BLUE, 25, width, height));		
		
		// Create a new thread using an anonymous class.
		// In this thread, repeat forever, calling
		// "repaint()" every 30 ms.
		repaintThread = new Thread() {                  // SOLUTION
			@Override                                   // SOLUTION
			public void run() {                         // SOLUTION
				while(true) {                           // SOLUTION
					repaint();                          // SOLUTION
					try {                               // SOLUTION
						Thread.sleep(30);               // SOLUTION
					} catch (InterruptedException e) {  // SOLUTION
					}                                   // SOLUTION
				}                                       // SOLUTION
			}                                           // SOLUTION
		};                                              // SOLUTION
		repaintThread.start();
	}

	/**
	 * This class only needs to override the "paint" method.
	 */
	private class ThingCanvas extends JPanel {
		/**
		 * The paint method, to draw all objects on the canvas
		 * 
		 * @param g
		 */
		@Override
		public void paint(Graphics g) {
			for (Thing thing : things) {  // SOLUTION
				thing.paint(g);           // SOLUTION
			}                             // SOLUTION
		}
	}
	
	private class Thing extends Thread {
		private Color color;
		private int size, maxX, maxY;
		private double x, y, speed, direction;
		
		private Thing(Color color, int size, int maxX, int maxY) {
			this.color = color;
			this.size = size;
			this.maxX = maxX - size;
			this.maxY = maxY - size;
			this.x = Math.random() * this.maxX;
			this.y = Math.random() * this.maxY;
			this.speed = Math.random();
			this.direction = Math.random() * 2 * Math.PI;
			this.start();
		}
		
		/**
		 * Draw this object using the "fillOval" method
		 * 
		 * @param g
		 */
		private void paint(Graphics g) {
			g.setColor(color);                         // SOLUTION
			g.fillOval((int) x, (int) y, size, size);  // SOLUTION
		}
		
		/**
		 * Move the object every 10ms
		 */
		@Override
		public void run() {
			while(true) {
				// Horizontal motion, wrap
				x += Math.cos(direction) * speed;
				if (x > maxX) x -= maxX;
				if (x < 0) x += maxX;

				// Vertical motion, bounce
				y += Math.sin(direction) * speed;
				if (y > maxY | y < 0) {
					direction = 2 * Math.PI - direction;
					if (y > maxY) {
						y = 2 * maxY - y;
					} else {
						y = 0 - y;
					}
				}
				
				// Delay 10ms
				try {
					Thread.sleep(10);
				} catch (InterruptedException e) {
				}
			}
		}
	}
}
