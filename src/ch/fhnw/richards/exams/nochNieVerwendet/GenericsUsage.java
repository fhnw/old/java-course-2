package ch.fhnw.richards.exams.nochNieVerwendet;

public class GenericsUsage {

	public static class Kiste<T> {
		T objekt;
		public Kiste(T objekt) {
			this.objekt = objekt;
		}
	}
	
	public static class Tier {	}
	public static class Grosstier extends Tier {	}	
	public static class Elefant extends Grosstier {	}
	public static class Kleintier extends Tier {	}
	public static class Hauskatze extends Kleintier {	}
	
	
	public GenericsUsage() {
		Kiste<Grosstier> k;
		k = new Kiste<Grosstier>(new Grosstier());
	}
}
