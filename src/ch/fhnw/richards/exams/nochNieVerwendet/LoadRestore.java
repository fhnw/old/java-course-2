package ch.fhnw.richards.exams.nochNieVerwendet;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class LoadRestore extends JFrame {
	private static final String fileName = "saveFile.xyz";

	int importantAAA = 1;
	String importantBBB = "Default string";
	MyObject importantCCC = new MyObject();

	class MyObject implements Serializable {
		String someInfo = "Some info";
		transient String otherInfo = "Other info";
	}

	/**
	 * This program automatically saves information to a file when it ends, and restored this
	 * information when it starts. If the file does not exist, no error should occur - the program
	 * will initialize normally without restored information.
	 */
	public static void main(String[] args) {
		new LoadRestore();
	}

	private LoadRestore() {
		super("Load & Restore exercise");
		restore();

		WindowAdapter x;
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				save();
				System.exit(0);
			}
		});

		displayGUI();
	}

	private void restore() {
		try {
			File f = new File(fileName);
			FileInputStream fs = new FileInputStream(f);
			ObjectInputStream in = new ObjectInputStream(fs);
			importantAAA = in.readInt();
			importantBBB = (String) in.readObject();
			importantCCC = (MyObject) in.readObject();
			in.close();
		} catch (Exception e) {
			// Do nothing - use default values
		}
	}

	private void save() {
		File f = new File(fileName);
		try {
			FileOutputStream fs = new FileOutputStream(f);
			ObjectOutputStream out = new ObjectOutputStream(fs);
			out.writeInt(importantAAA);
			out.writeObject(importantBBB);
			out.writeObject(importantCCC);
			out.flush();
			out.close();
		} catch (Exception e) {
			System.err.println("Unable to save data");
		}
	}

	private void displayGUI() {
		final JTextField txtImportantInt = new JTextField(Integer.toString(importantAAA));
		final JTextField txtImportantString = new JTextField(importantBBB);
		final JTextField txtObjectSomeInfo = new JTextField(importantCCC.someInfo);
		final JTextField txtObjectOtherInfo = new JTextField(importantCCC.otherInfo);
		JButton btnUpdate = new JButton("Update");

		this.setLayout(new GridLayout(5, 2, 10, 10));
		this.add(new JLabel("Important int:"));
		this.add(txtImportantInt);
		this.add(new JLabel("Important string:"));
		this.add(txtImportantString);
		this.add(new JLabel("Object some info:"));
		this.add(txtObjectSomeInfo);
		this.add(new JLabel("Object other info:"));
		this.add(txtObjectOtherInfo);

		this.add(new JLabel(""));
		Box buttonArea = Box.createHorizontalBox();
		this.add(buttonArea);
		buttonArea.add(Box.createHorizontalGlue());
		buttonArea.add(btnUpdate);

		btnUpdate.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				importantAAA = Integer.parseInt(txtImportantInt.getText());
				importantBBB = txtImportantString.getText();
				importantCCC.someInfo = txtObjectSomeInfo.getText();
				importantCCC.otherInfo = txtObjectOtherInfo.getText();
			}
		});

		pack();
		setVisible(true);
	}
}
