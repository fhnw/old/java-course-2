package ch.fhnw.richards.exams.midterm2013;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import javax.swing.JFileChooser;

/**
 * Program to read a file of numbers and test them against Benford's law.
 * Since this program is for use in an exam, it has been kept as short as
 * possible: No GUI, no error-checking, etc.
 */
public class Benford {

	public static void main(String[] args) {
		BufferedReader buffReader = null;
		int[] digits = new int[10]; // Digits 0 to 9
		
		try {
			// Get the input file
			JFileChooser chooser = new JFileChooser();
			chooser.showOpenDialog(null);		
			File file = chooser.getSelectedFile();
			
			// Set up the BufferedReader
			FileReader fileReader = new FileReader(file);
			buffReader = new BufferedReader(fileReader);
			
			// Count the first digits
			int nextChar = buffReader.read(); // read the first character
			boolean inNumber = false;
			while (nextChar >= 0) {
				boolean isDigit = (nextChar >= '0' && nextChar <= '9');
				if (isDigit && !inNumber) {
					// First digit found
					int digit = nextChar - '0';
					digits[digit]++;
					inNumber = true;
				} else if (!isDigit & inNumber) {
					inNumber = false;
				}
				nextChar = buffReader.read();
			}
		} catch (Exception e) {
			System.out.println("Error handling not implemented");
		} finally {
			try {
				buffReader.close();
			} catch (IOException e) {
			}
		}	
		
		// Print results
		for (int i = 1; i <= 9; i++) {
			System.out.println("Digit " + i + " occurs " + digits[i] + " times");
		}
	}
}
