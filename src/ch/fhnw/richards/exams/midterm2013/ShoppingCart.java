package ch.fhnw.richards.exams.midterm2013;

import java.math.BigDecimal;
import java.util.ArrayList;

public class ShoppingCart<T extends Product> {
	private ArrayList<Item<T>> items;
	private BigDecimal shippingCost;
	
	public ShoppingCart() {
		items = new ArrayList<>();
	}
	
	public void add(Item<T> item) {
		items.add(item);
	}
}
