package ch.fhnw.richards.exams.midterm2013;

import java.math.BigDecimal;

public class Item<T extends Product> {
	private T article;
	private int quantity;
	private BigDecimal price;
	
	public Item(T article, int quantity, BigDecimal price) {
		this.article = article;
		this. quantity = quantity;
		this.price = price;
	}
	
	public static <T extends Product> T getProductFromItem(Item<T> item) {
		return item.article;
	}
}
