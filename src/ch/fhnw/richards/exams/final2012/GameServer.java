package ch.fhnw.richards.exams.final2012;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Iterator;

public class GameServer extends Thread {
	// Declare the lobby - use generics correctly!
	private ArrayList<Player> lobby = new ArrayList<>();

	// Variable to control the ServerSocket loop
	private boolean stop = false;

	public static void main(String[] args) {
		GameServer server = new GameServer();
		server.start();
	}

	/**
	 * Typical ServerSocket implementation
	 */
	@Override
	public void run() {
		try {
			ServerSocket listener = new ServerSocket(50001);
			while (!stop) {
				// Wait for a client connection
				Socket socket = listener.accept();
				
				// Create a new player-object
				Player player = new Player(this, socket);
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}

	/**
	 * Method to add a player to the lobby. Can be called for new players joining the server, or for
	 * players who have just finished a game
	 * 
	 * @param playerToAdd
	 *          The player to add to the lobby
	 */
	protected void addToLobby(Player playerToAdd) {
		synchronized (lobby) {
			lobby.add(playerToAdd);
		}
	}

	/**
	 * Method to remove a player from the lobby. Can be called for players leaving the server, or for
	 * players entering a game. If no such player is in the lobby, the method returns false.
	 * 
	 * @param playerToRemove
	 *          T
	 * @return true if the player was found in the lobby
	 */
	protected boolean removeFromLobby(Player playerToRemove) {
		boolean success = false;

		synchronized (lobby) {
			// Use an iterator; search through the lobby for the player
			for (Iterator<Player> i = lobby.iterator(); i.hasNext();) {
				Player thisPlayer = i.next();
				if (thisPlayer == playerToRemove) {
					i.remove();
					success = true;
				}
			}
		}
		return success;
	}
}
