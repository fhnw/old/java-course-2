package ch.fhnw.richards.exams.final2012;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * This class uses a thread to receive messages from the client.
 * The client may send a message (example: "join game") at any time.
 * 
 * @author brad
 *
 */
public class Player implements Runnable {
	private GameServer server;
	private Socket socket;
	private boolean stopClient = false;
	private Thread receivingThread;
	private InputStreamReader in;
	private OutputStreamWriter out;
	
	public Player(GameServer server, Socket socket) {
		super();
		this.server = server;
		this.socket = socket;
		try {
			// Set up input-reader and output-writer
			this.in = new InputStreamReader(socket.getInputStream());
			this.out = new OutputStreamWriter(socket.getOutputStream());
			
			// Create a thread and start it
			receivingThread = new Thread(this);
			receivingThread.start();
			
			// Successfully created - add ourselves to the lobby
			server.addToLobby(this);			
		} catch (IOException e) {
		}
	}


	/**
	 * The run-method to read messages sent by the client
	 */
	@Override
	public void run() {
		while (!stopClient) {
			// Ignore the contents of this loop
		}
	}
	
	
	/**
	 * This method is called by the server to send a message to the
	 * client. The server may send messages at any times; hence, this
	 * is completely independent of receiving messages.
	 * 
	 * So that the server is not blocked, this method creates a temporary
	 * thread to send the message. **Important** This method must be
	 * synchronized. If the server tries to send messages very quickly,
	 * and the network is slow, we do not want two threads trying to send
	 * data at the same time.
	 * 
	 * @param msg
	 * @return true if no exception occurs
	 */
	public boolean sendMessage(Message msg) {
		return false;
	}
	
	public void stopClient() {
		// remove ourselves from the lobby
		server.removeFromLobby(this);
		
		// Shutdown I/O and the socket
		this.stopClient = true;
		receivingThread.interrupt();
		try { in.close();	} catch (IOException e) {	}
		try { out.close();	} catch (IOException e) {	}
		try { socket.close();	} catch (IOException e) {	}
		
	}
}
