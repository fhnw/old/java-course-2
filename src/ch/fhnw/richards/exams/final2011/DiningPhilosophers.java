package ch.fhnw.richards.exams.final2011;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;

public class DiningPhilosophers extends Thread {
	private static int minPhilosophers = 2;
	private static int maxPhilosophers = 5;
	private static int numTests = 10;
	private static String[] names = { "Kant", "Nietzsche", "Sartre", "Aristotle", "Plato" };

	private ArrayList<Table> tests = new ArrayList<Table>();;
	private int[] numDeadlocked = new int[maxPhilosophers - minPhilosophers + 1];

	public static void main(String[] args) {
		new DiningPhilosophers();
	}

	private DiningPhilosophers() {
		super("DiningPhilosophers");
		this.start();
	}

	private void wait(int ms) {
		try {
			sleep(ms);
		} catch (InterruptedException e) {
		}
	}

	@Override
	public void run() {
		for (int howMany = minPhilosophers; howMany <= maxPhilosophers; howMany++) {
			// Start the series of tests
			for (int test = 0; test < numTests; test++) {
				// Initialize this test
				Philosopher[] philosophers = new Philosopher[howMany];
				for (int i = 0; i < philosophers.length; i++) {
					philosophers[i] = new Philosopher(names[i]);
				}
				Table table = new Table(philosophers);
				tests.add(table);

				// Run test
				table.runTest();

				wait(10);
			}
		}
		// All tests have been started. We now monitor the list, and process the tests (tables) as they
		// finish
		for (int i = 0; i < numDeadlocked.length; i++) {
			numDeadlocked[i] = 0;
		}

		while (tests.size() > 0) {
			for (Iterator<Table> i = tests.iterator(); i.hasNext();) {
				Table test = i.next();
				if (test.isDone()) {
					if (test.isDeadlocked()) {
						numDeadlocked[test.getNumPhilosophers() - minPhilosophers]++;
					}
					i.remove();
				}
			}
			wait(100);
		}

		// Print results of this set of tests
		for (int i = 0; i < numDeadlocked.length; i++) {
			int num = numDeadlocked[i];
			int numPhilosophers = i + minPhilosophers;
			double percent = (num * 100.0) / numTests;
			System.out.println("Number of philosophers: " + numPhilosophers + ", percent deadlocked "
					+ percent);
		}
	}
}
