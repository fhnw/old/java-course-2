package ch.fhnw.richards.exams.final2011;

public class Philosopher extends Thread {
	public static int MaxBites = 100;
	public static int thinkTime = 10;
	private volatile int bitesLeft;
	private volatile boolean keepEating;
	Fork leftFork;
	Fork rightFork;
	
	Philosopher(String name) {
		this.setName(name);
		bitesLeft = MaxBites;
		keepEating = true;
	}
	
	void setLeftFork(Fork f) {
		leftFork = f;
	}
	
	void setRightFork(Fork f) {
		rightFork = f;
	}
	
	int getBitesLeft() {
		return bitesLeft;
	}
	
	void stopEating() {
		keepEating = false;
	}

	private void think() {
		try {
			sleep(thinkTime);
		} catch (InterruptedException e) {
		}
	}
	
	@Override
	public void run() {
		while (bitesLeft > 0 & keepEating) {
			// Try to take the forks
			leftFork.setInHand(this);
			rightFork.setInHand(this);
			
			// If we have two forks?
			if (leftFork.getInHand() == this & rightFork.getInHand() == this) {
				// Eat a bite
				bitesLeft--;
				
				// Put forks down
				leftFork.setInHand(null);
				rightFork.setInHand(null);				
			}
			
			// Think a while
			think();
		}
	}
}
