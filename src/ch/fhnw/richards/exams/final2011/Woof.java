package ch.fhnw.richards.exams.final2011;

public class Woof {
	private Integer lock;

	public static void main(String[] args) {
		new Woof();
	}
	
	public Woof() {
		lock = new Integer(13);
		(new Arf(lock)).start();
		(new Arf(lock)).start();
		(new Arf(lock)).start();
	}
	
	
	private class Arf extends Thread {
		private Integer lock;
		public Arf(Integer lock) {
			this.lock = lock;
		}
		@Override
		public void run() {
			wait(10);
			//synchronized(lock) {
			System.out.print("1");
			wait(100);
			System.out.print("2");
			//}
		}
		
		public void wait(int ms) {
			try {
				Thread.sleep(ms);
			} catch (InterruptedException e) {
			}
		}
	}
}
