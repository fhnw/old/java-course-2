package ch.fhnw.richards.exams.final2011;

public class OnlyOne {
	private static OnlyOne singleton;
	
	private int someField;
	
	public static OnlyOne OnlyOneFactory(int someField) {
		if (singleton == null) {
			singleton = new OnlyOne(someField);
		}
		return singleton;
	}
	
	private OnlyOne(int someField) {
		this.someField = someField;
	}
	
	public int getSomeField() {
		return someField;
	}
}
