package ch.fhnw.richards.exams.final2011;

public enum ClientActions {
	CONNECT(String.class),
	JOIN(Integer.class),
	PLAY(Card.class),
	CHAT(String.class);
	
	private Class<?> info;
	
	private ClientActions(Class<?> c) {
		info = c;
	}
	
	public Class<?> getInfo() {
		return info;
	}
}
