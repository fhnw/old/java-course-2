package ch.fhnw.richards.exams.final2011;

public class Fork {
	private Philosopher inHand;
	
	protected Fork() {
		inHand = null;
	}
	
	/**
	 * Called by a philosopher to take or release a fork. Releasing a fork always
	 * works. Taking a fork only works if the fork is not currently in someone's hand.
	 */
	protected synchronized void setInHand(Philosopher p) {
		if (p == null | inHand == null) {
			inHand = p;
		}
	}
	
	protected Philosopher getInHand() {
		return inHand;
	}
}
