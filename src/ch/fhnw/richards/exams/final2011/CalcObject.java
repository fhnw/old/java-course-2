package ch.fhnw.richards.exams.final2011;

public class CalcObject {
	private float value;
	
	public CalcObject(float value) {
		this.value = value;
	}
	
	public void add(float a) {
		value = value + a;
	}
	public void multiply(float a) {
		value = value * a;
	}
	public void divide(float a) {
		value = value / a;
	}
	public void subtract(float a) {
		value = value - a;
	}
	
	public float getValue() {
		return value;
	}
}
