package ch.fhnw.richards.exams.final2011;

public class Table extends Thread {	
	Philosopher[] philosophers;
	Fork[] forks;	
	private boolean isDeadlocked;
	private boolean isDone;
	
	Table(Philosopher[] philosophers) {
		super("Table");
		this.philosophers = philosophers;
		int length = philosophers.length;
		
		// Create the forks
		forks = new Fork[length];
		for (int i = 0; i < length; i++) {
			forks[i] = new Fork();
		}
		
		// Place the forks between the philosophers
		for (int i = 0; i < length; i++) {
			Philosopher p = philosophers[i];
			int leftFork = i;
			int rightFork = (i+1) % length;
			p.setLeftFork(forks[leftFork]);
			p.setRightFork(forks[rightFork]);
		}
		
		isDeadlocked = false;
		isDone = false;
	}
	
	boolean isDeadlocked() {
		return isDeadlocked;
	}
	
	boolean isDone() {
		return isDone;
	}
	
	int getNumPhilosophers() {
		return philosophers.length;
	}
	
	/**
	 * If we cared about exactness, this would need to be synchronized.
	 * We do not care, so it isn't.
	 */
	private int getTotalBitesLeft() {
		int totalBitesLeft = 0;
		for (Philosopher p : philosophers) {
			totalBitesLeft += p.getBitesLeft();
		}
		return totalBitesLeft;
	}
	
	void runTest() {
		for (Philosopher p : philosophers) {
			p.start();
		}
		this.start();
	}
	
	@Override
	public void run() {
		int lastBitesLeft;
		int thisBitesLeft = Integer.MAX_VALUE;
		while(!isDone & thisBitesLeft > 0) {
			lastBitesLeft = thisBitesLeft;
			thisBitesLeft = getTotalBitesLeft();
			if (lastBitesLeft == thisBitesLeft) {
				isDeadlocked = true;
				// Deadlock detected - kill philosophers
				for (Philosopher p : philosophers) {
					p.stopEating();
				}
				isDone = true;
			}
			try {
				sleep(500);
			} catch (InterruptedException e) {
			}
		}
		
		// Send the philosophers home (release the objects for garbage collection)
		for (int i = 0 ; i < philosophers.length; i++) {
			philosophers[i] = null;
		}
		
		isDone = true;
	}
}
