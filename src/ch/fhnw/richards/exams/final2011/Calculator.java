package ch.fhnw.richards.exams.final2011;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class Calculator {
	private CalcObject thing;
	private float[] numbers;
	private char[] operators;
	
	public static void main(String[] args) {
		// Split the input string on "space" to separate operators and numbers
		String formula = args[0];
		String[] items = formula.split(" ");
		
		// Create the calculator object
		Calculator calc = new Calculator(items);
		
		// Do the calculations and print the result
		calc.calculate();
		calc.printResult();
	}

	private Calculator(String[] items) {
		// Create the MathThing object using the first value
		thing = new CalcObject(Float.parseFloat(items[0]));
		
		// Create an array of values (float) and an array of operators (char)
		int numItems = items.length/2;
		numbers = new float[numItems];
		operators = new char[numItems];
		for (int i = 1 ; i < items.length; i+=2) {
			operators[i/2] = items[i].charAt(0); // should only ever be one character 
			numbers[i/2] = Float.parseFloat(items[i+1]);
		}		
	}
	
	private void calculate() {
		String methodName = null;
		for (int i = 0; i < numbers.length; i++) {
			switch (operators[i]) {
			case '+':
				methodName = "add";
				break;
			case '-':
				methodName = "subtract";
				break;
			case '*':
				methodName = "multiply";
				break;
			case '/':
				methodName = "divide";
				break;
			}
			
			try {
				Method m = thing.getClass().getMethod(methodName, float.class);
				m.invoke(thing, numbers[i]);
			} catch (Exception e) {
				System.out.println("Oh, no! " + e.toString());
			}
		}
	}
	
	private void printResult() {
		System.out.println("The result is " + thing.getValue());		
	}
}
