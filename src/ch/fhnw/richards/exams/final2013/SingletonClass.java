package ch.fhnw.richards.exams.final2013;

public class SingletonClass {
	
	private static SingletonClass singleton;
	
	private long timeCreated;
	
	private SingletonClass() {
		timeCreated = System.currentTimeMillis();
	}
	
	public static SingletonClass makeSingleton() {
		if (singleton == null) {
			singleton = new SingletonClass();
		}
		return singleton;
	}

}
