package ch.fhnw.richards.exams.final2013;

public class SingletonTest {

	public static void main(String[] args) {
		// Create an instance of SingletonClass
		SingletonClass s1 = SingletonClass.makeSingleton();
		
		// Try to create a second instance of SingletonClass
		SingletonClass s2 = SingletonClass.makeSingleton();
		
		// The two instances should be the same
		if (s1 != s2) System.out.println("Error!");
	}

}
