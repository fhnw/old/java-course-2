package ch.fhnw.richards.exams.final2013;

public class ThreadTest {

	public static void main(String[] args) {
		ThreadTest tm = new ThreadTest();
		for (int i = 1; i <= 10; i++) {
			tm.doCalculation(i);
		}
	}
	
	/**
	 * This method calls the private method veryLongCalculation, but places the
	 * execution of that method in a separate thread. As soon as the thread is
	 * started, this method returns and normal program execution continues.
	 * 
	 * This method uses an anonymous class to create the thread.
	 * 
	 * @param n A parameter that must be passed to veryLongCalculation()
	 */
	public void doCalculation(final int n) {
		Thread calculator = new Thread() {
			@Override
			public void run() {
				veryLongCalculation(n);
			}
		};
		calculator.start();
	}

	/**
	 * What this method does is unimportant. Important is only that the
	 * calculation takes a very long time...
	 */
	private void veryLongCalculation(int n) {
		int max = n;
		while (max < Integer.MAX_VALUE / 10)
			max *= 10;
		long answer = 0;

		while (answer < Integer.MAX_VALUE - n) {
			long x = n;
			for (int i = 0; i < n; i++) {
				x++;
			}
			Long increment = x / n;
			answer += increment;
		}

		System.out.println("Task finished, answer = " + answer);
	}
}
