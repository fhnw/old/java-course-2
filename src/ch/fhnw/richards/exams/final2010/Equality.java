package ch.fhnw.richards.exams.final2010;

public class Equality<T> {
	T obj1;
	T obj2;
	public Equality(T obj1, T obj2) {
		this.obj1 = obj1;
		this.obj2 = obj2;
	}
	public boolean test() {
		boolean a1 = (obj1.equals(obj2));
		boolean a2 = (obj1 == obj2);
		return a1 == a2;
	}
}
