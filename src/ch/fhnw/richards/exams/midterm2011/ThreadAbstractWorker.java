package ch.fhnw.richards.exams.midterm2011;

import java.util.LinkedList;

import ch.fhnw.richards.exams.midterm2011.ThreadTester.Element;

public abstract class ThreadAbstractWorker implements Runnable {
	protected LinkedList<Element> list;
	private long sleepTime;
	private Thread thread;
	private boolean stop;
	
	public ThreadAbstractWorker(LinkedList<Element> list) {
		super();
		this.list = list;
		sleepTime = 100; // default value; should be set by subclass
		stop = false;
		this.thread = new Thread(this);
		thread.start();
	}

	public void stopThread() {
		stop = true;
		thread.interrupt();
	}
	
	protected void setSleepTime(long millis) {
		if (millis < 1) millis = 1;
		this.sleepTime = millis;
	}
	
	@Override
	public void run() {
		while (!stop) {
			synchronized(list) {
				doWork();
			}
			try {
				Thread.sleep(sleepTime);
			} catch (InterruptedException e) {
			}
		}
	}
	
	protected abstract  void doWork();
}
