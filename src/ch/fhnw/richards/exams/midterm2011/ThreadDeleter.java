package ch.fhnw.richards.exams.midterm2011;

import java.util.LinkedList;

import ch.fhnw.richards.exams.midterm2011.ThreadTester.Element;

public class ThreadDeleter extends ThreadAbstractWorker {
	
	public ThreadDeleter(LinkedList<Element> list) {
		super(list);
	}

	@Override
	protected void doWork() {
		long size = list.size();
		int position = (int) (Math.random() * (double) size);
		if (position < (size-1)) {
			try {
				Element elt = list.remove(position);
				elt.delete();
			} catch (Exception e) {
				System.out.println("---> Exception deleting element");
			}
		}
		this.setSleepTime(1000 / list.size());
	}	
}
