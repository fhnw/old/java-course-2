package ch.fhnw.richards.exams.midterm2011;

import java.util.Date;

public class CurrencyData {
	
	private static final GoldPrice[] EurPrices =
	{
			new GoldPrice("EUR", new Date(111,0,1), 1048.16),
			new GoldPrice("EUR", new Date(110,4,3), 883.65),
			new GoldPrice("EUR", new Date(109,3,20), 664.65),
			new GoldPrice("EUR", null, 531.09)
	};

	private static final GoldPrice[] ChfPrices =
	{
			new GoldPrice("CHF", new Date(111,0,1), 1347.60),
			new GoldPrice("CHF", new Date(109,9,11), 1082.72),
			new GoldPrice("CHF", null, 895.85)
	};

	private static final GoldPrice[] UsdPrices =
	{
			new GoldPrice("USD", new Date(111,0,1), 1558.30),
			new GoldPrice("USD", new Date(110,6,4), 1211.6),
			new GoldPrice("USD", new Date(108,11,26), 848.0),
			new GoldPrice("USD", null, 666.41)
	};
	
	public static GoldPrice loadHistoricalGoldPrice(String iso, int index) {
		if ("EUR".equals(iso)) {
			return EurPrices[index];
		} else if ("CHF".equals(iso)) {
			return ChfPrices[index];
		} else if ("USD".equals(iso)) {
			return UsdPrices[index];
		} else {
			return null;
		}
	}
}
