package ch.fhnw.richards.exams.midterm2011;

import java.util.Date;

public class GoldPrice {
	private String isoCurrency;
	private Date validFrom;
	private double pricePerOunce;

	public GoldPrice(String isoCurrency, Date validFrom, double pricePerOunce) {
		this.isoCurrency = isoCurrency;
		this.validFrom = validFrom;
		this.pricePerOunce = pricePerOunce;
	}

	public String getIsoCurrency() {
		return isoCurrency;
	}

	public Date getValidFrom() {
		return validFrom;
	}

	public double getPricePerOunce() {
		return pricePerOunce;
	}
}
