package ch.fhnw.richards.exams.midterm2011;

import java.util.Date;

public class CurrencyTester {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Currency euros = new Currency("EUR");
		Currency francs = new Currency("CHF");
		Currency dollars = new Currency("USD");

		System.out.println("Year-Mt   EUR/Ounce   CHF/Ounce   USD/Ounce     CHF/USD     CHF/EUR");
		Date d = new Date();
		int year = d.getYear()+1900;
		int month = d.getMonth()+1;
		double goldEuro = euros.getGoldPrice();
		double goldChf = francs.getGoldPrice();
		double goldUsd = dollars.getGoldPrice();
		double ChfUsd = francs.getExchangeRate(dollars);
		double ChfEur = francs.getExchangeRate(euros);
		System.out.printf("%4d-%02d   %9.2f   %9.2f   %9.2f   %9.2f   %9.2f%n", year, month, goldEuro, goldChf, goldUsd, ChfUsd, ChfEur);
		
		for (int monthsAgo = 1; monthsAgo < 40; monthsAgo++) {
			d = new Date(111, 4-monthsAgo, 1);
			year = d.getYear()+1900;
			month = d.getMonth()+1;
			goldEuro = euros.getGoldPrice(d);
			goldChf = francs.getGoldPrice(d);
			goldUsd = dollars.getGoldPrice(d);
			ChfUsd = francs.getExchangeRate(dollars, d);
			ChfEur = francs.getExchangeRate(euros, d);
			System.out.printf("%4d-%02d   %9.2f   %9.2f   %9.2f   %9.2f   %9.2f%n", year, month, goldEuro, goldChf, goldUsd, ChfUsd, ChfEur);
		}
	}

}
