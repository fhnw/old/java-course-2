package ch.fhnw.richards.exams.midterm2011;

import java.util.ArrayList;
import java.util.Date;

import com.sun.xml.internal.bind.v2.runtime.reflect.ListIterator;

public class Currency {
	// ISO currency identified ("CHF", "EUR", "USD", etc.)
	private String currencyISO;

	// Historical exchange rates for all dates in the past
	private TimeRateList historicalRates;

	protected Currency(String currencyISO) {
		this.currencyISO = currencyISO;
		historicalRates = loadHistoricalGoldPrices(currencyISO);
	}

	private TimeRateList loadHistoricalGoldPrices(String iso) {
		// Data loaded from a database - contents unimportant for exam...
		TimeRateList history = new TimeRateList();
		boolean done = false;
		int i = 0;
		while (!done) {
			GoldPrice gp = CurrencyData.loadHistoricalGoldPrice(iso, i);
			history.add(gp);
			done = (gp.getValidFrom() == null);
			i++;
		}
		return history;
	}

	/**
	 * Return the current price of gold in this currency
	 * 
	 * @return the current price of gold in this currency
	 */
	public double getGoldPrice() {
		return historicalRates.get(0).getPricePerOunce();
	}

	/**
	 * Return the price of gold as of a particular date
	 * 
	 * @param date - the date on which we want the gold price
	 * @return the price of gold as of a particular date
	 */
	public double getGoldPrice(Date date) {
		return historicalRates.get(date).getPricePerOunce();
	}
	
	/**
	 * Return the ISO code for this currency
	 * 
	 * @return the ISO code for this currency
	 */
	public String getISO() {
		return currencyISO;
	}

	/**
	 * Return the current exchange-rate against the given currency object, i.e., how much (in our currency)
	 * one unit of the other currency costs
	 * 
	 * @param Currency - the other currency
	 * @return the price of one unit of the other currency
	 */
	public double getExchangeRate(Currency otherCurrency) {
		double ourGold = this.getGoldPrice();
		double otherGold = otherCurrency.getGoldPrice();
		return ourGold / otherGold;
	}

	/**
	 * Return a historical exchange-rate against the given currency object
	 * 
	 * @param Currency - the other currency
	 * @return the price of one unit of the other currency on the given date
	 */
	public double getExchangeRate(Currency otherCurrency, Date date) {
		double ourGold = this.getGoldPrice(date);
		double otherGold = otherCurrency.getGoldPrice(date);
		return ourGold / otherGold;
	}

	/**
	 * This class is an adaptation of ArrayList. Each entry in the list is an instance of GoldPrice.
	 * 
	 * A TimeRateList is sorted when it is created. The first element is the most recent (containing
	 * the current price), The second element contains an earlier date, the third is even earlier. The
	 * last element always contains a <null> date, meaning that it's rate is used for all dates prior
	 * to the date of the next-to-last element.
	 * 
	 * Elements are accessed by date - returning the element with the last date that is still before
	 * the date specified. For example, suppose we have a TimeRateList containing elements dated
	 * 17-Oct-2010, 31-Jul-2010, 15-Jun-2010, and <null>. If we are asked for the exchange rate as of
	 * 13-Aug-2010, we will use the rate from 31-Jul-2010. If we are asked for the rate from
	 * 3-Jun-2010, we will use the rate from the last <null> element.
	 */
	private class TimeRateList extends ArrayList<GoldPrice> {
		private TimeRateList() {
			super();
		}

		/**
		 * Return the GoldPrice that applies for the given date
		 * 
		 * @param onDate - the date on which we want to know the price
		 * @return the GoldPrice object that contains the price
		 */
		private GoldPrice get(Date onDate) {
			GoldPrice useThisPrice = null;
			for (GoldPrice elt : this) {
				if (elt.getValidFrom() == null) {
					useThisPrice = elt;
					break;
				} else if (onDate.after(elt.getValidFrom())) {
					useThisPrice = elt;
					break;
				}
			}
			return useThisPrice;
		}
	}
}
