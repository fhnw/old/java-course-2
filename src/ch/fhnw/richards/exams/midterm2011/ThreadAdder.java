package ch.fhnw.richards.exams.midterm2011;

import java.util.LinkedList;

import ch.fhnw.richards.exams.midterm2011.ThreadTester.Element;

public class ThreadAdder extends ThreadAbstractWorker {

	public ThreadAdder(LinkedList<Element> list) {
		super(list);
	}

	@Override
	protected void doWork() {
		long size = list.size();
		Element elt = new Element();
		int position = (int) (Math.random() * (double) size);
		try {
		list.add(position, elt);
	} catch (Exception e) {
		System.out.println("---> Exception adding element");
	}
		this.setSleepTime(list.size());
	}	
}
