package ch.fhnw.richards.exams.midterm2011;

import java.util.LinkedList;

import ch.fhnw.richards.exams.midterm2011.ThreadTester.Element;

public class ThreadChecker extends ThreadAbstractWorker {
	
	public ThreadChecker(LinkedList<Element> list) {
		super(list);
		this.setSleepTime(500);
	}

	@Override
	protected void doWork() {
		long size = list.size();
		Element lastElement = list.getLast();
		boolean valid = lastElement.isLast();
		System.out.printf("Number of elements: %3d; last elt valid: %b%n", size, valid);
		
		System.gc();
	}	
}
