package ch.fhnw.richards.exams.midterm2011;

import java.util.LinkedList;
import java.util.Date;

public class ThreadTester {
	protected LinkedList<Element> list = new LinkedList<Element>();
	protected long listSize = 0;

	public static void main(String[] args) {
		new ThreadTester();
	}

	public ThreadTester() {
		// Set up the list by creating the final element
		list.add(new Element(true));
		new ThreadChecker(list);

		for (int i = 0; i < 20; i++) {
			new ThreadAdder(list);
			new ThreadDeleter(list);
		}
	}

	protected static class Element {
		private long timestamp;
		private boolean isLastElement;
		private boolean isDeleted;

		public Element() {
			this.timestamp = new Date().getTime();
			this.isLastElement = false;
			this.isDeleted = false;
		}

		public Element(boolean isLast) {
			this.timestamp = (new Date()).getTime();
			this.isLastElement = isLast;
			this.isDeleted = false;
		}

		@Override
		public void finalize() {
			if (!isDeleted) {
				System.out.println("---> Element lost");
			}
		}

		public long getTimestamp() {
			return timestamp;
		}

		public boolean isLast() {
			return isLastElement;
		}

		public boolean isDeleted() {
			return isDeleted;
		}

		public void delete() {
			isDeleted = true;
		}
	}
}
