package ch.fhnw.richards.exams.final2009;

public class SortTest {
	public static void main(String[] args) {
		int[] numbers;
		numbers = new int[args.length];
		for (int i = 0; i < args.length; i++) {
			numbers[i] = Integer.parseInt(args[i]);
		}
		print(numbers);
		sort(numbers);
		print(numbers);
		reverse(numbers);
		print(numbers);
	}

	public static void print(int[] numbers) {
		for (int n : numbers) {
			System.out.print(n + " ");
		}
		System.out.println();
	}

	// Simple in-place selection sort
	public static void sort(int[] numbers) {
		for (int i = 0; i < numbers.length-1; i++) {
			for (int j = i+1; j < numbers.length; j++) {
				if (numbers[i] > numbers[j]) {
					swap(numbers, i, j);
				}
			}
		}		
	}
	
	// Simple in-place reverse
	public static void reverse(int[] numbers) {
		for (int i = 0; i < numbers.length/2; i++) {
			int j = numbers.length-i-1;
			swap(numbers, i, j);
		}
	}
	
	// Swap elements i and j
	private static void swap(int numbers[], int i, int j) {
		int tmp = numbers[i];
		numbers[i] = numbers[j];
		numbers[j] = tmp;
	}
}
