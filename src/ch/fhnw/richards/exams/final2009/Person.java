package ch.fhnw.richards.exams.final2009;

public class Person implements PersonInterface {
	private String name;
	private String vorname;

	public Person(String name, String vorname) {
		this.name = name;
		this.vorname = vorname;
	}

	public void print() {
		System.out.println("Nachname: " + name);
		System.out.println("Vorname: " + vorname);
	}
}
