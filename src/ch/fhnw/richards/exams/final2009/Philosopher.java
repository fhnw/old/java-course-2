package ch.fhnw.richards.exams.final2009;

public class Philosopher extends Thread {
	private String name;
	private Fork leftFork;
	private Fork rightFork;
	private int bites;

	public Philosopher(String name, Fork leftFork, Fork rightFork) {
		super();
		this.name = name;
		this.leftFork = leftFork;
		this.rightFork = rightFork;
		this.bites = 0;
	}

	@Override
	public void run() {
		while (bites < 10) {
			// get left fork, if we do not already have it
			if (leftFork.heldBy() != this) {
				synchronized (leftFork) {
					if (leftFork.onTable()) {
						leftFork.take(this);
					}
				}
			}

			// If we have the left fork, try to take the right one
			if (leftFork.heldBy() == this) {
				synchronized (rightFork) {
					if (rightFork.onTable()) {
						rightFork.take(this);
					}
				}
			}

			// if have both forks then take a bite
			if (leftFork.heldBy() == this & rightFork.heldBy() == this) {
				System.out.println(name + " takes bite " + bites);
				bites += 1;
				rightFork.release();
				leftFork.release();
			} else {
				System.out.print('.');
			}

			try {
				Thread.sleep(100 * (int) (Math.random() + 1.0));
			} catch (InterruptedException e) {
			}
		}
	}
}
