package ch.fhnw.richards.exams.final2009;

// "heldBy" says which philosopher is holding this fork
// If the fork is on the table, "heldBy" is null
public class Fork {
	private int number;
	private Philosopher heldBy;

	public Fork(int number) {
		this.number = number;
	}

	public void take(Philosopher p) {
		heldBy = p;
	}

	public void release() {
		heldBy = null;
	}

	public boolean onTable() {
		return (heldBy == null);
	}

	public Philosopher heldBy() {
		return heldBy;
	}
}
