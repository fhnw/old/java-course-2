package ch.fhnw.richards.exams.final2009;

public class DiningPhilosophers {
	private Fork[] forks;
	private Philosopher[] philosophers;
	private static String[] names = { "James", "Ralph", "Simon", "David", "Jonas" };

	public static void main(String[] args) {
		new DiningPhilosophers();
	}

	public DiningPhilosophers() {
		forks = new Fork[5];
		philosophers = new Philosopher[5];
		for (int i = 0; i < 5; i++) {
			forks[i] = new Fork(i);
		}
		for (int i = 0; i < 5; i++) {
			int left = i;
			int right = i + 1;
			if (right < 5) {
				// the case for four of the philosophers: the right
				// fork has a number one higher than the left fork
			} else {
				// one philosopher has forks 4 and 0.
				right = 0;
			}
			philosophers[i] = new Philosopher(names[i], forks[left], forks[right]);
			philosophers[i].start();
		}
	}
}
