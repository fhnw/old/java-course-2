package ch.fhnw.richards.exams.final2009;

public class CompanyParts {
	
}

class Part {
	private int serialNumber;
	
	public Part() {
	}
}

class Pump extends Part {
	private float weight;
	private float operatingPressure;
	private float capacity;
	
	public Pump() {
		super();
	}
}

class MembranePump extends Pump {
	private MembraneMaterial membrane;
	
	public MembranePump() {
		super();
	}
}

class RotaryPump extends Pump {
	private int numberOfVanes;
	
	public RotaryPump() {
		super();
	}
}

class Tank extends Part {
	float volume;
	
	public Tank() {
		super();
	}
}

enum MembraneMaterial {
	NITRILE, VITON
}