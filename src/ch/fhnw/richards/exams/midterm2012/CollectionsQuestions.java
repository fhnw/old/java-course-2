package ch.fhnw.richards.exams.midterm2012;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Stack;
import java.util.TreeSet;

public class CollectionsQuestions {

	public static void main(String[] args) {
		question6();

	}

	public static void question1() {
		LinkedList<Integer> list = new LinkedList<Integer>();
		list.add(13);
		list.add(7);
		list.add(27);
		list.add(7);

		Iterator<Integer> it = list.iterator();
		while (it.hasNext()) {
			int elt = it.next();
			System.out.print(elt + " ");
		}
		System.out.println();
	}

	public static void question2() {
		TreeSet<Integer> tree = new TreeSet<Integer>();
		tree.add(13);
		tree.add(7);
		tree.add(27);
		tree.add(7);

		for (Integer elt : tree) {
			System.out.print(elt + " ");
		}
		System.out.println();
	}

	public static void question3() {
		ArrayList<Integer> list = new ArrayList<Integer>();
		list.add(13);
		list.add(7);
		list.add(27);
		list.add(7);

		Iterator<Integer> it = list.iterator();
		while (it.hasNext()) {
			int elt = it.next();
			System.out.print(elt + " ");
		}
		System.out.println();
	}

	public static void question4() {
		HashMap<Integer, String> hashtable = new HashMap<Integer, String>();
		hashtable.put(13, "a");
		hashtable.put(7, "b");
		hashtable.put(27, "c");
		hashtable.put(7, "d");

		Collection<String> values = hashtable.values();
		for (String elt : values) {
			System.out.print(elt + " ");
		}
		System.out.println();
	}

	public static void question5() {
		HashMap<Integer, String> hashtable = new HashMap<Integer, String>();
		hashtable.put(13, "a");
		hashtable.put(7, "b");
		hashtable.put(27, "c");
		hashtable.put(19, "d");
		hashtable.remove(7);

		Collection<String> values = hashtable.values();
		for (String elt : values) {
			System.out.print(elt + " ");
		}
		System.out.println();
	}

	public static void question6() {
		Stack<Integer> stack = new Stack<Integer>();
		stack.push(13);
		stack.push(7);
		stack.push(27);
		stack.push(7);

		while (!stack.empty()) {
			int elt = stack.pop();
			System.out.print(elt + " ");
		}
	}
}
