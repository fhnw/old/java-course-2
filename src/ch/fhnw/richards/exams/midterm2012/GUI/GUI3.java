package ch.fhnw.richards.exams.midterm2012.GUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;

public class GUI3 extends JFrame {
	JLabel lblMain = new JLabel("Empty");
	JButton btnShowX = new JButton("Show X");
	JButton btnShowY = new JButton("Show Y");
	JButton btnRed = new JButton("Red");
	JButton btnBlue = new JButton("Blue");

	public static void main(String[] args) {
		new GUI3();
	}

	private GUI3() {
		this.setLayout(new BorderLayout());
		this.add(lblMain, BorderLayout.CENTER);
		lblMain.setOpaque(true);
		lblMain.setHorizontalAlignment(SwingConstants.CENTER);
		lblMain.setPreferredSize(new Dimension(200,200));

		JPanel toolbar = new JPanel();
		toolbar.setLayout(new GridLayout(1,4));
		this.add(toolbar, BorderLayout.SOUTH);

		toolbar.add(btnShowX);
		toolbar.add(btnShowY);
		toolbar.add(btnRed);
		toolbar.add(btnBlue);

		addEventHandling();

		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		pack();
		setVisible(true);
	}

	private void addEventHandling() {
		btnRed.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lblMain.setBackground(Color.RED);
			}
		});
		btnBlue.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				lblMain.setBackground(Color.BLUE);
			}			
		});
		
		btnHandler XY_handler = new btnHandler();
		btnShowX.addActionListener(XY_handler);
		btnShowY.addActionListener(XY_handler);
	}

	private class btnHandler implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent e) {
			if (e.getSource() == btnShowX) {
				lblMain.setText("X");
			} else { // must be btnShowY
				lblMain.setText("Y");
			}
		}
	}
}
