package ch.fhnw.richards.network.webServer2;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.*;
import java.net.*;
import java.util.*;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;



public class WebServer2 extends javax.swing.JFrame {
	Set<ClientConnection2> connections; // set of currently open connections
	Listener2 serverThread; // the thread we will use for the server
	final int maxConnections = 10;

	// GUI stuff
	private javax.swing.JScrollPane jScrollPane;
	private javax.swing.JTextArea txtLog;
	private javax.swing.JLabel lblPort;
	private javax.swing.JTextField txtPort;
	private javax.swing.JButton btnGo;

	/** Creates new form WebServer2 */
	public static void main(String args[]) {
		new WebServer2();
	}
	
	public WebServer2() {
		this.setTitle("WebServer2");
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());

		Box topBox = Box.createHorizontalBox();
		pane.add(topBox, BorderLayout.NORTH);

		lblPort = new JLabel("Port");
		topBox.add(lblPort);
		txtPort = new JTextField("80");
		txtPort.setPreferredSize(new Dimension(150, 21));
		topBox.add(txtPort);
		btnGo = new JButton("Go");
		topBox.add(btnGo);
		btnGo.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				serveContent(evt);
			}
		});

		jScrollPane = new javax.swing.JScrollPane();
		jScrollPane.setPreferredSize(new Dimension(300, 300));
		txtLog = new JTextArea();
		txtLog.setFont(new Font("Courier New", 0, 12));
		jScrollPane.setViewportView(txtLog);
		pane.add(jScrollPane, BorderLayout.CENTER);

		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				endServer();
			}
		});

		pack();
		setVisible(true);
	}

	private void endServer() {
		btnGo.setEnabled(false);
		serverThread.stopListening();
		for (ClientConnection2 c : connections) {
			c.interrupt();
		}
		
		// Wait a bit, to give the threads a chance to exit
		for ( int count = 0;	count < 100; count++) {
			boolean somethingAlive = false;
			if (serverThread.isAlive() ) somethingAlive = true;
			for (ClientConnection2 c : connections) {
				if (c.isAlive()) somethingAlive = true;
			}
			if (!somethingAlive) break; // all done...
			log("Shutting down - waiting for threads to end " + count);
			this.repaint();
		}
		System.exit(0);
	}
	private void serveContent(java.awt.event.MouseEvent evt) {
		if (serverThread != null && serverThread.isAlive()) {
			serverThread.stopListening();
			btnGo.setText("Go");
		} else {
			try {
				connections = new HashSet<ClientConnection2>(maxConnections);
				serverThread = new Listener2(this, Integer.parseInt(txtPort.getText()));
				serverThread.start();
			} catch (Exception e) {
				log(e.toString());
			}
			btnGo.setText("Stop");
		}
	}

	protected void addConnection(Socket s) {
		// Note: for 100% correctness, we could synchronize the call to connections.size().
		// However, being slightly off here is of little importance...
		if (connections.size() >= maxConnections) {
			try {
				PrintWriter outClient = new PrintWriter(s.getOutputStream());
				outClient.print("Server busy - connection refused\n");
				outClient.flush();
				outClient.close();
				s.close();
				log("ClientConnection:" + s.getInetAddress().getHostAddress() + ":" + s.getPort());
			} catch (Exception e) {
				log(e.toString());
			}
		} else {
			ClientConnection2 c = new ClientConnection2(this, s);
			synchronized(connections) {
				connections.add(c);
			}
			c.start();
		}
	}
	
	protected void removeConnection(ClientConnection2 c) {
		synchronized(connections) {
			connections.remove(c);
		}
	}

	void log(String in) {
		String newText = txtLog.getText() + "\n" + in;
		txtLog.setText(newText);
	}

}
