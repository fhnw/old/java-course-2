package ch.fhnw.richards.network.mathServer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.Thread;
import java.net.Socket;

/**
 * Logic module to process an incoming connection. We handle this in a thread,
 * because the connection could be used for multiple requests. In our simple
 * example, this is not true - hence, there is no loop in the run method.
 */
public class MathLogic extends Thread {
	Socket socket;
	BufferedReader reader;
	OutputStreamWriter writer;

	public MathLogic(Socket socket) {
		this.socket = socket;
		try {
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new OutputStreamWriter(socket.getOutputStream());
		} catch (IOException e) {
		}
	}

	@Override
	public void run() {
		try {
			MathMessage msg = new MathMessage(reader.readLine());

			int answer = 0;
			switch (msg.getOperator()) {
				case '+':
					answer = msg.getOperand1() + msg.getOperand2();
					break;
				case '-':
					answer = msg.getOperand1() - msg.getOperand2();
					break;
			}

			writer.write(answer + "\n");
			writer.flush(); // push answer through to the server
			
			// Clean up when finished
			reader.close();
			writer.close();
			socket.close();
		} catch (IOException e) {
			System.out.println(e.toString());
		}
	}
}
