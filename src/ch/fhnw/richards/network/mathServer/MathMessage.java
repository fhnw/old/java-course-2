package ch.fhnw.richards.network.mathServer;

/**
 * This is a very simple message class. In a real application:
 * 
 * - The operators should be an enumerated type, so that it is impossible to
 * choose an unsupported operator
 * 
 * - The message format should not be a string; XML would be a better choice.
 */
public class MathMessage {
	private int operand1;
	private int operand2;
	private char operator;

	/**
	 * Create a MathMessage from the basic components
	 */
	public MathMessage(int o1, char op, int o2) {
		operand1 = o1;
		operand2 = o2;
		operator = op;
	}
	
	/**
	 * Create a MathMessage from an incoming string
	 */
	public MathMessage(String messageIn) {
		String[] tokens = messageIn.split(" ");
		operand1 = Integer.parseInt(tokens[0]);
		operand2 = Integer.parseInt(tokens[2]);
		operator = tokens[1].toCharArray()[0];		
	}
	
	@Override
	public String toString() {
		return operand1 + " " + operator + " " + operand2;
	}

	//----- Getters -----

	public int getOperand1() {
		return operand1;
	}

	public int getOperand2() {
		return operand2;
	}

	public char getOperator() {
		return operator;
	}	
}
