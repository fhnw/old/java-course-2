package ch.fhnw.richards.network.mathServer;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * This class simulates a business-logic server. It accepts requests from clients,
 * processing each request in a separate thread. The individual requests follow
 * a very simple protocol: they transmit a simple mathematical formula like "2 + 2".
 * The server parses this formula, calculate and returns the result.
 * 
 * The code has been kept as simple as possible - no GUI, no error handling, etc..
 * A real application should add these, use a better message format, such as XML.
 */
public class MathServer extends Thread {

	public static void main(String[] args) {
		MathServer server = new MathServer();
		server.start();
	}

	@Override
	public void run() {
		try {
			ServerSocket listener = new ServerSocket(50001);
			while(true) {
				Socket s = listener.accept(); // wait for request
				
				// Create a MathLogic-object to process the request
				MathLogic logic = new MathLogic(s);
				logic.start();
			}
		} catch (IOException e) {
			System.out.println(e.toString());
		}
		
	}
}
