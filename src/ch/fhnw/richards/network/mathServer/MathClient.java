package ch.fhnw.richards.network.mathServer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

/**
 * This class simulates a business-client. It sents to a business-logic server,
 * and awaits an answer. The individual requests follow a very simple protocol:
 * They are simple mathematical formulae like "2 + 2". The server parses this
 * formula, calculate and returns the result. The client sends a single request,
 * tests the answer, then quits.
 * 
 * The code has been kept as simple as possible - no GUI, no error handling, etc.
 */
public class MathClient {
	Socket socket;
	BufferedReader reader;
	OutputStreamWriter writer;

	public static void main(String[] args) {
		MathClient c = new MathClient();
		c.testRequest();
	}

	private void testRequest() {
		try {
			socket = new Socket("127.0.0.1", 50001);
			reader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			writer = new OutputStreamWriter(socket.getOutputStream());
			
			int x = (int) (Math.random() * 10);
			int y = (int) (Math.random() * 10);
			MathMessage msg = new MathMessage(x, '+', y);
			
			writer.write(msg.toString() + "\n");
			writer.flush();
			
			String answerString = reader.readLine();
			int answer = Integer.parseInt(answerString);
			
			System.out.println(msg.toString() + " = " + answer);
			
			reader.close();
			writer.close();
			socket.close();
		} catch (Exception e) {
			System.out.println(e.toString());
		}
	}
}
