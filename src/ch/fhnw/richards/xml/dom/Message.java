package ch.fhnw.richards.xml.dom;

import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.StringWriter;
import java.net.Socket;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import ch.fhnw.richards.xml.simpleXML.Message.MessageType;

public class Message {
	private Document xmlDocument;
	private String xmlString; // The xmlDocument as a string

	public enum MessageType {
		Hello, NewClient, NewClientAccepted, Goodbye
	};

	// Static names of elements and attributes that we use
	private final String ELEMENT_MESSAGE = "message";
	private final String ELEMENT_TYPE = "type";
	private final String ELEMENT_NAME = "name";
	private final String ELEMENT_AGE = "age";
	private final String ATTR_ID = "id";
	private final String ATTR_TIMESTAMP = "timestamp";

	// Data included in a message
	private long id;
	private long timestamp;
	private MessageType type;
	private String name;
	private Integer age;

	// Generator for a unique message ID
	private static long messageID = 0;

	/**
	 * Increment the global messageID
	 * 
	 * @return the next valid ID
	 */
	private static long nextMessageID() {
		return messageID++;
	}

	/**
	 * Receive a message: create a message object and fill it using data
	 * transmitted over the given socket.
	 * 
	 * @param socket
	 *            the socket to read from
	 * @return the new message object, or null in case of an error
	 * @throws Exception
	 */
	public static Message receive(Socket socket) throws Exception {
		Message msg = null;
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document xmlDocument = builder.parse(new InputSource(socket.getInputStream()));
		msg = new Message(xmlDocument);
		return msg;
	}

	/**
	 * Constructor to accept and disassemble existing xmlDocuments.
	 */
	private Message(Document xmlDocument) {
		Element root = null;
		Element type = null;
		Element name = null;
		Element age = null;

		this.xmlDocument = xmlDocument;
		root = xmlDocument.getDocumentElement();
		this.id = Long.parseLong(root.getAttribute(ATTR_TIMESTAMP));
		String timestamp = root.getAttribute(ATTR_TIMESTAMP);
		if (timestamp != null) this.timestamp = Long.parseLong(timestamp);

		NodeList tmpElements = root.getElementsByTagName(ELEMENT_TYPE);
		if (tmpElements.getLength() > 0) {
			 type = (Element) tmpElements.item(0);
			this.type = parseType(type.getTextContent());
		}

		tmpElements = root.getElementsByTagName(ELEMENT_NAME);
		if (tmpElements.getLength() > 0) {
			name = (Element) tmpElements.item(0);
			this.name = name.getTextContent();
		}

		tmpElements = root.getElementsByTagName(ELEMENT_AGE);
		if (tmpElements.getLength() > 0) {
			age = (Element) tmpElements.item(0);
			this.age = Integer.parseInt(age.getTextContent());
		}
	}

	private MessageType parseType(String typeName) {
		MessageType type = null;
		if (MessageType.Hello.toString().equals(typeName)) {
			type = MessageType.Hello;
		} else if (MessageType.Goodbye.toString().equals(typeName)) {
			type = MessageType.Goodbye;
		} else if (MessageType.NewClient.toString().equals(typeName)) {
			type = MessageType.NewClient;
		} else if (MessageType.NewClientAccepted.toString().equals(typeName)) {
			type = MessageType.NewClientAccepted;
		}
		return type;
	}

	/**
	 * Create a new message
	 * 
	 * @param type
	 *            What type of message
	 */
	public Message(MessageType type) {
		this.id = nextMessageID();
		xmlDocument = null; // Not yet constructed
		xmlString = null; // Not yet converted
		this.type = type;

	}

	/**
	 * If the message has not yet been constructed from the attributes, do so...
	 */
	private void buildMessage() {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		try {
			DocumentBuilder builder = factory.newDocumentBuilder();
			this.xmlDocument = builder.newDocument();
			Element eltMessage = xmlDocument.createElement(ELEMENT_MESSAGE);
			xmlDocument.appendChild(eltMessage);
			eltMessage.setAttribute(ATTR_ID, Long.toString(this.id));
			eltMessage.setAttribute(ATTR_TIMESTAMP,
					Long.toString(System.currentTimeMillis()));

			// Create the <type> node
			Element eltType = xmlDocument.createElement(ELEMENT_TYPE);
			eltType.setTextContent(type.toString());
			eltMessage.appendChild(eltType);

			// For anything other than Hello and Goodbye, we have additional
			// nodes
			if (type != MessageType.Hello && type != MessageType.Goodbye) {

				// Create the <name> node
				Element eltName = xmlDocument.createElement(ELEMENT_NAME);
				eltName.setTextContent(name);
				eltMessage.appendChild(eltName);

				// If needed, create the <age> node
				if (this.age != null) {
					Element eltAge = xmlDocument.createElement(ELEMENT_AGE);
					eltAge.setTextContent(age.toString());
					eltMessage.appendChild(eltAge);
				}
			}
		} catch (ParserConfigurationException e) {
		}
	}

	/**
	 * Convert to a String representation
	 * 
	 * @return String representation of an XML document
	 */
	@Override
	public String toString() {
		String xmlOut = null;
		if (xmlDocument == null) buildMessage();

		if (xmlString == null) {
			try { // Ignore all sorts of possible exceptions...

				// Set up a transformer that will convert from DOM to XML-text
				TransformerFactory factory = TransformerFactory.newInstance();
				Transformer transformer = factory.newTransformer();
				transformer.setOutputProperty(OutputKeys.INDENT, "yes");

				// Set up the output stream
				StringWriter out = new StringWriter();

				// XML transformer requires special input/output classes
				DOMSource source = new DOMSource(xmlDocument);
				StreamResult result = new StreamResult(out);

				// Finally: send the XML to the output stream
				transformer.transform(source, result);
				xmlString = out.toString();
			} catch (Exception e) {

			}
		}
		return xmlString;
	}

	/**
	 * Send the message
	 * 
	 * @param s
	 *            The socket to use when sending the message
	 */
	public void send(Socket s) {
		String xmlOut = this.toString();

		try { // Ignore IO errors
			OutputStreamWriter out = new OutputStreamWriter(s.getOutputStream());
			out.write(xmlOut);
			out.flush();
		} catch (Exception e) {
		}
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public static long getMessageID() {
		return messageID;
	}

	public long getId() {
		return id;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public MessageType getType() {
		return type;
	}
}
