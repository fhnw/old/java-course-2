package ch.fhnw.richards.xml.dom;

	import java.awt.BorderLayout;
	import java.awt.Container;
	import java.io.BufferedReader;
	import java.io.IOException;
	import java.io.InputStreamReader;
	import java.io.OutputStreamWriter;
	import java.net.Socket;

	import javax.swing.Box;
	import javax.swing.JButton;
	import javax.swing.JFrame;
	import javax.swing.JLabel;
	import javax.swing.JScrollPane;
	import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import ch.fhnw.richards.xml.dom.Message.MessageType;

	public class MessageClient extends JFrame {
		private JLabel lblIP = new JLabel("IP");
		private JTextField txtIP = new JTextField();
		private JLabel lblPort = new JLabel("Port");
		private JTextField txtPort = new JTextField();
		private JButton btnGo;

		private Box boxSend;
		private JLabel lblSend = new JLabel("Message to send");
		private JScrollPane paneSend;
		private JTextArea txtSend;

		private Box boxReceive;
		private JLabel lblReceive = new JLabel("Last message received");
		private JScrollPane paneReceive;
		private JTextArea txtReceive;

		private Socket socket; // socket for communications
		StringBuffer buf = new StringBuffer();

		public static void main(String[] args) {
			new MessageClient();
		}

		/** Creates new form Browser1 */
		public MessageClient() {
			this.setTitle("Message client");
			Container pane = getContentPane();
			pane.setLayout(new BorderLayout());

			Box topBox = Box.createHorizontalBox();
			pane.add(topBox, BorderLayout.NORTH);

			topBox.add(lblIP);

			txtIP.setText("127.0.0.1");
			txtIP.setPreferredSize(new java.awt.Dimension(200, 21));
			topBox.add(txtIP);

			topBox.add(Box.createHorizontalStrut(30));

			topBox.add(lblPort);

			txtPort.setText("8080");
			txtPort.setPreferredSize(new java.awt.Dimension(50, 21));
			topBox.add(txtPort);

			topBox.add(Box.createHorizontalStrut(30));

			btnGo = new JButton("Go");
			topBox.add(btnGo);
			btnGo.addMouseListener(new java.awt.event.MouseAdapter() {
				public void mouseClicked(java.awt.event.MouseEvent evt) {
					btnGoMouseClicked(evt);
				}
			});

			// Set up send-area
			paneSend = new JScrollPane();
			paneSend.setPreferredSize(new java.awt.Dimension(300, 400));
			txtSend = new JTextArea();
			txtSend.setFont(new java.awt.Font("Courier New", 0, 12));
			paneSend.setViewportView(txtSend);
			boxSend = Box.createVerticalBox();
			boxSend.add(lblSend);
			boxSend.add(paneSend);
			pane.add(boxSend, BorderLayout.WEST);

			// Set up receive-area
			paneReceive = new JScrollPane();
			paneReceive.setPreferredSize(new java.awt.Dimension(300, 400));
			txtReceive = new JTextArea();
			txtReceive.setFont(new java.awt.Font("Courier New", 0, 12));
			paneReceive.setViewportView(txtReceive);
			boxReceive = Box.createVerticalBox();
			boxReceive.add(lblReceive);
			boxReceive.add(paneReceive);
			pane.add(boxReceive, BorderLayout.EAST);

			// Handle window-closing event
			addWindowListener(new java.awt.event.WindowAdapter() {
				public void windowClosing(java.awt.event.WindowEvent evt) {
					exitForm(evt);
				}
			});
			
			// Important note
			StringBuilder sb = new StringBuilder();
			sb.append("this does not work, because DOM\n");
			sb.append("expects to receive a single\n");
			sb.append("document, and then have the\n");
			sb.append("input connection closed. It\n");
			sb.append("blocks at the parse method\n");
			sb.append("and goes no farther.");
			txtReceive.setText(sb.toString());

			pack();
			setVisible(true);
		}

		/** Exit the Application */
		private void exitForm(java.awt.event.WindowEvent evt) {
			try {
				if (socket != null) socket.close();
			} catch (Exception e) {
			}
			System.exit(0);
		}

		private void btnGoMouseClicked(java.awt.event.MouseEvent evt) {
			Thread t = new Thread() {
				@Override
				public void run() {
					// Network errors are always possible
					try {
						socket = new Socket(txtIP.getText(), new Integer(txtPort.getText()));

						// Send a "Hello" message
						final Message helloMessage = new Message(MessageType.Hello);
						txtSend.setText(txtSend.getText() + "\n\n" + helloMessage.toString());
						helloMessage.send(socket);
						
						// Receive a "Hello" reply
						Message reply = Message.receive(socket);
						txtReceive.setText(txtReceive.getText() + "\n\n" + reply.toString());
						
						// Send a "NewClient" message
						Message newClientMessage = new Message(MessageType.NewClient);
						newClientMessage.setName("Roger Rabbit");
						newClientMessage.setAge(3);
						txtSend.setText(txtSend.getText() + "\n\n" + newClientMessage.toString());
						newClientMessage.send(socket);
						
						// Receive a "NewClientAccepted" reply
						reply = Message.receive(socket);
						txtReceive.setText(txtReceive.getText() + "\n\n" + reply.toString());
						
						// Send a "Goodbye" message
						Message goodbyeMessage = new Message(MessageType.Goodbye);
						txtSend.setText(txtSend.getText() + "\n\n" + goodbyeMessage.toString());
						goodbyeMessage.send(socket);
						
						// Receive a "Goodbye" reply
						reply = Message.receive(socket);
						txtReceive.setText(txtReceive.getText() + "\n\n" + reply.toString());
						
					} catch (Exception e) {
						txtReceive.setText("ERROR: " + e.toString());
					}
				}
			};
			t.start();
		}					
	}