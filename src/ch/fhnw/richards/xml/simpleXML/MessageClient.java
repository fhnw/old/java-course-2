package ch.fhnw.richards.xml.simpleXML;

import java.awt.BorderLayout;
import java.awt.Container;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class MessageClient extends JFrame {
	private JLabel lblIP = new JLabel("IP");
	private JTextField txtIP = new JTextField();
	private JLabel lblPort = new JLabel("Port");
	private JTextField txtPort = new JTextField();
	private JButton btnGo;
	private JScrollPane paneInhalt;
	private JTextArea txtInhalt;

	public static void main(String[] args) {
		new MessageClient();
	}

	/** Creates new form Browser1 */
	public MessageClient() {
		this.setTitle("Message client");
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());
		
		Box topBox = Box.createHorizontalBox();
		pane.add(topBox, BorderLayout.NORTH);
		
		topBox.add(lblIP);

		txtIP.setText("127.0.0.1");
		txtIP.setPreferredSize(new java.awt.Dimension(200, 21));
		topBox.add(txtIP);
		
		topBox.add(Box.createHorizontalStrut(30));

		topBox.add(lblPort);

		txtPort.setText("8080");
		txtPort.setPreferredSize(new java.awt.Dimension(50, 21));
		topBox.add(txtPort);

		topBox.add(Box.createHorizontalStrut(30));

		btnGo = new JButton("Go");
		topBox.add(btnGo);
		btnGo.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnGoMouseClicked(evt);
			}
		});

		paneInhalt = new JScrollPane();
		paneInhalt.setPreferredSize(new java.awt.Dimension(600, 300));
		txtInhalt = new JTextArea();
		txtInhalt.setFont(new java.awt.Font("Courier New", 0, 12));
		paneInhalt.setViewportView(txtInhalt);
		pane.add(paneInhalt, BorderLayout.CENTER);

		// Handle window-closing event
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				exitForm(evt);
			}
		});

		pack();
		setVisible(true);
	}

	/** Exit the Application */
	private void exitForm(java.awt.event.WindowEvent evt) {
		System.exit(0);
	}

	private void btnGoMouseClicked(java.awt.event.MouseEvent evt) {
		StringBuffer result = new StringBuffer();
		
		// Network errors are always possible
		try {
			Socket s = new Socket(txtIP.getText(), new Integer(txtPort.getText()));
			
			// Send a "hello" message, receive an answer, and display it
			Message msgOut = new Message(Message.MessageType.Hello);
			msgOut.send(s);
			Message msgIn = Message.receive(s);
			result.append(msgIn.toString() + "\n"); 
			txtInhalt.setText(result.toString());
			
			// Send a "NewClient" message
			msgOut = new Message(Message.MessageType.NewClient);
			msgOut.setName("Jennifer Jumpingjacks");
			msgOut.setAge(23);
			msgOut.send(s);
			msgIn = Message.receive(s);
			result.append(msgIn.toString() + "\n"); 
			txtInhalt.setText(result.toString());
			
			// Send a "goodbye" message
			msgOut = new Message(Message.MessageType.Goodbye);
			msgOut.send(s);
			msgIn = Message.receive(s);
			result.append(msgIn.toString() + "\n"); 
			txtInhalt.setText(result.toString());
			
			s.close();
		}

		// If an error occurred, show the error message in txtInhalt
		catch (Exception err) {
			txtInhalt.setText("ERROR: " + err.toString());
		}
	}
}
