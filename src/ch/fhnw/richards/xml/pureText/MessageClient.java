package ch.fhnw.richards.xml.pureText;

import java.awt.BorderLayout;
import java.awt.Container;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

public class MessageClient extends JFrame {
	private JLabel lblIP = new JLabel("IP");
	private JTextField txtIP = new JTextField();
	private JLabel lblPort = new JLabel("Port");
	private JTextField txtPort = new JTextField();
	private JButton btnGo;

	private Box boxSend;
	private JLabel lblSend = new JLabel("Message to send");
	private JScrollPane paneSend;
	private JTextArea txtSend;
	private JButton btnSend = new JButton("Send");

	private Box boxReceive;
	private JLabel lblReceive = new JLabel("Last message received");
	private JScrollPane paneReceive;
	private JTextArea txtReceive;

	private Socket socket; // socket for communications
	StringBuffer buf = new StringBuffer();

	public static void main(String[] args) {
		new MessageClient();
	}

	/** Creates new form Browser1 */
	public MessageClient() {
		this.setTitle("Message client");
		Container pane = getContentPane();
		pane.setLayout(new BorderLayout());

		Box topBox = Box.createHorizontalBox();
		pane.add(topBox, BorderLayout.NORTH);

		topBox.add(lblIP);

		txtIP.setText("127.0.0.1");
		txtIP.setPreferredSize(new java.awt.Dimension(200, 21));
		topBox.add(txtIP);

		topBox.add(Box.createHorizontalStrut(30));

		topBox.add(lblPort);

		txtPort.setText("8080");
		txtPort.setPreferredSize(new java.awt.Dimension(50, 21));
		topBox.add(txtPort);

		topBox.add(Box.createHorizontalStrut(30));

		btnGo = new JButton("Go");
		topBox.add(btnGo);
		btnGo.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				btnGoMouseClicked(evt);
			}
		});

		// Set up send-area
		paneSend = new JScrollPane();
		paneSend.setPreferredSize(new java.awt.Dimension(300, 400));
		txtSend = new JTextArea();
		txtSend.setFont(new java.awt.Font("Courier New", 0, 12));
		paneSend.setViewportView(txtSend);
		boxSend = Box.createVerticalBox();
		boxSend.add(lblSend);
		boxSend.add(paneSend);
		boxSend.add(btnSend);
		pane.add(boxSend, BorderLayout.WEST);

		btnSend.addMouseListener(new java.awt.event.MouseAdapter() {
			public void mouseClicked(java.awt.event.MouseEvent evt) {
				// send whatever the user entered - maybe it is even XML!
				try {
					OutputStreamWriter out = new OutputStreamWriter(socket.getOutputStream());
					out.write(txtSend.getText());
					out.flush();
				} catch (IOException e) {
					txtReceive.setText("ERROR: " + e.toString());
				}
			}
		});

		// Set up receive-area
		paneReceive = new JScrollPane();
		paneReceive.setPreferredSize(new java.awt.Dimension(300, 400));
		txtReceive = new JTextArea();
		txtReceive.setFont(new java.awt.Font("Courier New", 0, 12));
		paneReceive.setViewportView(txtReceive);
		boxReceive = Box.createVerticalBox();
		boxReceive.add(lblReceive);
		boxReceive.add(paneReceive);
		pane.add(boxReceive, BorderLayout.EAST);

		// Handle window-closing event
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent evt) {
				exitForm(evt);
			}
		});

		pack();
		setVisible(true);
	}

	/** Exit the Application */
	private void exitForm(java.awt.event.WindowEvent evt) {
		try {
			if (socket != null) socket.close();
		} catch (Exception e) {
		}
		System.exit(0);
	}

	private void btnGoMouseClicked(java.awt.event.MouseEvent evt) {
		// Network errors are always possible
		try {
			socket = new Socket(txtIP.getText(), new Integer(txtPort.getText()));

			// Start a thread that reads from the stream
			Thread receiver = new Thread() {
				@Override
				public void run() {
					try {
						InputStreamReader in = new InputStreamReader(socket.getInputStream());

						int i;
						while ((i = in.read()) != -1) { // Read answer char-by-char
							buf.append((char) i);
							txtReceive.setText(buf.toString()); // Show result as we read..
						}
					} catch (IOException e) {
						txtReceive.setText("ERROR: " + e.toString());
					}
				}
			};
			receiver.start();
		} catch (Exception e) {
			txtReceive.setText("ERROR: " + e.toString());
		}
	}
}
