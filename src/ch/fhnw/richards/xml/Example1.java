package ch.fhnw.richards.xml;

import java.io.File;

import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class Example1 {

	public static void main(String[] args) {
		new Example1();
	}

	public Example1() {
		DocumentBuilderFactory f = DocumentBuilderFactory.newInstance();
		try {
			Document d = f.newDocumentBuilder().parse(new File("src/ch/fhnw/richards/xml/woof.xml"));
			
			NodeList elts = d.getElementsByTagName("dog");
			int numNodes = elts.getLength();
			for (int i = 0; i < numNodes; i++) {
				Node elt = elts.item(i);
				System.out.println(elt.toString());
				NamedNodeMap attrs = elt.getAttributes();
				for (int j = 0; j < attrs.getLength(); j++) {
					Node attr = attrs.item(j);
					System.out.println("-- " + attr.toString());
					attr.getTextContent(); // TODO
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
