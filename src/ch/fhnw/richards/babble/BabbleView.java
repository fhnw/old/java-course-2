package ch.fhnw.richards.babble;

import java.awt.BorderLayout;
import java.awt.Dimension;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;
import javax.swing.border.EmptyBorder;

/**
 * This is the "View" part of an MVC solution for the "Babble" exercise.
 * 
 * The GUI works reasonably nicely, but as always, any GUI I design is
 * basically ugly...oh well...
 * 
 * @author brad
 */
public class BabbleView extends JFrame {
	// These fields are "package private" to allow direct access by BabbleController
	JButton btnInputFile = new JButton("Choose input file");
	JLabel lblInputFile = new JLabel();
	
	JButton btnOutputFile = new JButton("Choose output file");
	JLabel lblOutputFile = new JLabel();
	JSlider sWindowSize = new JSlider(1, 7);
	JButton btnBabble = new JButton("Babble");
	TextPanel pnlInputFile = new TextPanel("Input file", 300);
	TextPanel pnlAnalysis = new TextPanel("Analysis", 100);
	TextPanel pnlOutputFile = new TextPanel("Generated output", 300);

	/**
	 * The main method creates instances of each of the MVC classes
	 * 
	 * @param args - not used
	 */
	public static void main(String[] args) {
		BabbleView gui = new BabbleView();
		BabbleModel logic = new BabbleModel();
		BabbleController controller = new BabbleController(gui, logic);
	}
	
	/**
	 * Create the GUI
	 */
	private BabbleView() {
		super("Babble");
		this.setLookAndFeel(); // Something prettier than standard "Java Ugly"
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		// Start on the GUI
		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.LINE_AXIS));
		
		// Input file area
		Box boxArea = Box.createVerticalBox();
		this.add(boxArea);
		boxArea.add(Box.createVerticalStrut(15));
		Box tmpBox = Box.createHorizontalBox();
		tmpBox.add(Box.createHorizontalStrut(15));
		tmpBox.add(btnInputFile);
		tmpBox.add(Box.createHorizontalGlue());
		boxArea.add(tmpBox);
		boxArea.add(Box.createVerticalStrut(15));
		tmpBox = Box.createHorizontalBox();
		tmpBox.add(Box.createHorizontalStrut(15));
		tmpBox.add(lblInputFile);
		tmpBox.add(Box.createHorizontalGlue());
		boxArea.add(tmpBox);
		boxArea.add(Box.createVerticalStrut(15));
		boxArea.add(pnlInputFile);
		

		// Analysis area
		boxArea = Box.createVerticalBox();
		this.add(boxArea);
		boxArea.add(Box.createVerticalStrut(15));
		
		boxArea.add(new JLabel("Window size"));
		sWindowSize.setMajorTickSpacing(1);
		sWindowSize.setPaintTicks(true);
		sWindowSize.setPaintLabels(true);
		sWindowSize.setMaximumSize(new Dimension(150,50));
		boxArea.add(sWindowSize);
		boxArea.add(pnlAnalysis);
		
		// Output file area
		boxArea = Box.createVerticalBox();
		this.add(boxArea);
		boxArea.add(Box.createVerticalStrut(15));
		tmpBox = Box.createHorizontalBox();
		tmpBox.add(Box.createHorizontalStrut(15));
		tmpBox.add(btnOutputFile);
		tmpBox.add(Box.createHorizontalStrut(15));
		tmpBox.add(Box.createHorizontalGlue());
		btnBabble.setEnabled(false);
		tmpBox.add(btnBabble);
		tmpBox.add(Box.createHorizontalStrut(15));
		boxArea.add(tmpBox);
		boxArea.add(Box.createVerticalStrut(15));
		tmpBox = Box.createHorizontalBox();
		tmpBox.add(Box.createHorizontalStrut(15));
		tmpBox.add(lblOutputFile);
		tmpBox.add(Box.createHorizontalGlue());
		boxArea.add(tmpBox);
		boxArea.add(Box.createVerticalStrut(15));
		boxArea.add(pnlOutputFile);
		
		
		this.pack();
		this.setVisible(true);
	}
	
	/**
	 * A simple class containing a JTextArea in a JScrollPane, with a
	 * title (JLabel) above.
	 * 
	 * @author brad
	 *
	 */
	public class TextPanel extends JPanel {
		private JTextArea txtArea;
		private JScrollPane scrollPane;
		
		/**
		 * Create a panel instance
		 * 
		 * @param title The title of the panel
		 * @param minWidth The minimum width of the panel
		 */
		public TextPanel(String title, int minWidth) {
			super();
			this.setLayout(new BorderLayout());
			this.setBorder(new EmptyBorder(5,5,5,5));
			this.add(new JLabel(title), BorderLayout.NORTH);
			txtArea = new JTextArea();
			scrollPane = new JScrollPane(txtArea);
			scrollPane.setMinimumSize(new Dimension(minWidth,300));
			scrollPane.setPreferredSize(new Dimension(2 * minWidth,600));
			this.add(scrollPane, BorderLayout.CENTER);
		}
		
		/**
		 * Method to get the text from the enclosed JTextArea
		 * 
		 * @return The displayed text
		 */
		public String getText() {
			return txtArea.getText();
		}
		
		/**
		 * Method to set the text in the enclosed JTextArea
		 * 
		 * @param content The text to display
		 */
		public void setText(String content) {
			txtArea.setText(content);
		}
	}

	/**
	 * A simple method from the Java Tutorials, to set the Nimbus look-and-feel
	 */
	private void setLookAndFeel() {
		try {
		    for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
		        if ("Nimbus".equals(info.getName())) {
		            UIManager.setLookAndFeel(info.getClassName());
		            break;
		        }
		    }
		} catch (Exception e) {
		}		
	}	
}
