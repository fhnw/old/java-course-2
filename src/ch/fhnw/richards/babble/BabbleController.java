package ch.fhnw.richards.babble;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

/**
 * This is the "Controller" part of an MVC solution for the "Babble" exercise.
 * 
 * @author brad
 */
public class BabbleController {
	private final BabbleView gui;
	private final BabbleModel logic;
	private final JFileChooser jfc = new JFileChooser();
	private int windowSize;
	private boolean analyzed = false;
	private String inputText = null;
	private boolean analysisComplete = false;
	private String outputText = null;
	private File outputFile = null;

	/**
	 * Creates the controller, which communicates between the View and the Model.
	 * The main task of this constructor is to hook up all of the event listeners.
	 * 
	 * @param guiIn The view
	 * @param logicIn The model
	 */
	public BabbleController(BabbleView guiIn, BabbleModel logicIn) {
		this.gui = guiIn;
		this.logic = logicIn;
		windowSize = gui.sWindowSize.getValue();

		// Prevent the window size from being too small
		gui.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				int width = gui.getWidth();
				int height = gui.getHeight();
				if (width < 800) width = 800;
				if (height < 450) height = 450;
				gui.setSize(width, height);
			}
		});

		// When an input file is selected, reset the analysis and output
		gui.btnInputFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if (jfc.showOpenDialog(gui) == JFileChooser.APPROVE_OPTION) {
					File inputFile = jfc.getSelectedFile();
					gui.lblInputFile.setText(inputFile.getPath());
					inputText = null;
					setAnalysisComplete(false);
					if (checkInputFile(inputFile)) {
						inputText = logic.loadInputFile(inputFile);
						if (inputText != null) {
							if (logic.analyze(inputText, windowSize))
								setAnalysisComplete(true);
						}
					}
					gui.pnlInputFile.setText(inputText);
				}
			}
		});

		// An output file must be selected in order to "babble"
		gui.btnOutputFile.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				gui.btnBabble.setEnabled(false);
				if (jfc.showSaveDialog(gui) == JFileChooser.APPROVE_OPTION) {
					File outputFile = jfc.getSelectedFile();
					gui.lblOutputFile.setText(outputFile.getPath());
					outputText = null;
					gui.lblOutputFile.setText(jfc.getSelectedFile().getPath());
					if (checkOutputFile(outputFile)) {
						gui.btnBabble.setEnabled(analysisComplete);
						gui.pnlOutputFile.setText(null);
						BabbleController.this.outputFile = outputFile;
					}
				}
			}
		});

		// Changing the slider automatically re-runs the analysis,
		// as long as the input text has been loaded
		gui.sWindowSize.addChangeListener(new ChangeListener() {
			@Override
			public void stateChanged(ChangeEvent e) {
				setAnalysisComplete(false);
				windowSize = gui.sWindowSize.getValue();
				if (inputText != null) {
					if (logic.analyze(inputText, windowSize)) setAnalysisComplete(true);
				}
			}
		});

		// "Babble" generates output, which is displayed and also
		// written to the selected file.
		gui.btnBabble.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				outputText = logic.babble();
				gui.pnlOutputFile.setText(outputText);
				logic.writeOutputFile(outputFile, outputText);
			}
		});

	}

	// Utility method: if the status of the analysis changes,
	// either reset or finished, we need to change several
	// controls.
	private void setAnalysisComplete(boolean isComplete) {
		analysisComplete = isComplete;
		if (isComplete) {
			gui.pnlAnalysis.setText(logic.getAnalysisString());
		}
		gui.pnlOutputFile.setText(null);
		gui.btnBabble.setEnabled(isComplete & (outputFile != null));
	}

	/**
	 * Check the status of an input file
	 * 
	 * @param fileIn The file to check
	 * @return status: true if the file is ok
	 */
	private boolean checkInputFile(File fileIn) {
		boolean allOk = true;

		if (!fileIn.exists()) { // does not exist
			JOptionPane.showMessageDialog(gui, "Input file does not exist.",
					"Error", JOptionPane.ERROR_MESSAGE);
			allOk = false;
		}
		if (allOk && !fileIn.isFile()) { // is a directory
			JOptionPane.showMessageDialog(gui, "Input file is a directory.",
					"Error", JOptionPane.ERROR_MESSAGE);
			allOk = false;
		}
		if (allOk && !fileIn.canRead()) { // Not readable
			JOptionPane.showMessageDialog(gui, "Input file cannot be read.",
					"Error", JOptionPane.ERROR_MESSAGE);
			allOk = false;
		}

		return allOk;
	}

	/**
	 * Check the status of an output file
	 * 
	 * @param fileOut The file to check
	 * @return status: true if the file is ok
	 */
	private boolean checkOutputFile(File fileOut) {
		boolean allOk = true;

		String directoryName = fileOut.getParent();
		if (directoryName == null) { // No path, so use the current directory
			directoryName = System.getProperty("user.dir");
		}
		File directory = new File(directoryName);
		if (!directory.exists()) { // does not exist
			JOptionPane.showMessageDialog(gui,
					"Output directory does not exist.", "Error",
					JOptionPane.ERROR_MESSAGE);
			allOk = false;
		}
		if (allOk && !directory.canWrite()) { // Not writeable
			JOptionPane.showMessageDialog(gui,
					"Output directory is not writeable.", "Error",
					JOptionPane.ERROR_MESSAGE);
			allOk = false;
		}

		return allOk;
	}
}
