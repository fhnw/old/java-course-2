package ch.fhnw.richards.babble;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Random;
import java.util.TreeSet;

import javax.swing.JOptionPane;

/**
 * This is the "Model" part of an MVC solution for the "Babble" exercise.
 * 
 * @author brad
 */
public class BabbleModel {
	private HashMap<String, HashMap<String, Integer>> analysis;
	private final char BOF = '\u0152';
	private final char EOF = '\u0153';
	private String firstKey = null;
	private Random rand = new Random();

	/**
	 * Load an input file from disk
	 * 
	 * @param inputFile The file to load
	 * @return The contents of the file
	 */
	public String loadInputFile(File inputFile) {
		StringBuffer textIn = new StringBuffer();
		String textOut = null;
		char[] charBuffer = new char[1024];
		Reader in = null;
		try {
			in = new BufferedReader(new FileReader(inputFile));
			int numChars = in.read(charBuffer);
			while (numChars > -1) {
				textIn.append(charBuffer, 0, numChars);
				numChars = in.read(charBuffer);
			}
			textOut = textIn.toString();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.toString(), "Error reading file",
					JOptionPane.ERROR_MESSAGE);
		} finally {
			if (in != null) try {
				in.close();
			} catch (IOException e) {
			}

		}
		return textOut;
	}

	/**
	 * Write "babble" output to disk
	 * 
	 * @param outputFile The file to use
	 * @param outputText The output to write
	 */
	public void writeOutputFile(File outputFile, String outputText) {
		Writer out = null;
		try {
			out = new FileWriter(outputFile);
			out.write(outputText);
			out.flush();
		} catch (Exception e) {
			JOptionPane.showMessageDialog(null, e.toString(), "Error writing file",
					JOptionPane.ERROR_MESSAGE);
		} finally {
			if (out != null) try {
				out.close();
			} catch (IOException e) {
			}
		}
	}

	/**
	 * Analyze the input text, to prepare for "babble" generation
	 * 
	 * @param inputText The text to analyze
	 * @param windowSize The window-size for the analysis
	 * @return
	 */
	public boolean analyze(String inputText, int windowSize) {
		analysis = new HashMap<String, HashMap<String, Integer>>();

		String fullText = BOF + inputText + EOF;
		firstKey = fullText.substring(0, windowSize);
		for (int i = 0; i < fullText.length() - windowSize; i++) {
			String key = fullText.substring(i, i + windowSize);
			HashMap<String, Integer> nextChars;
			String nextChar = fullText.substring(i + windowSize, i + windowSize + 1);
			if (analysis.containsKey(key)) {
				// Get the existing hashtable of entries
				nextChars = analysis.get(key);
				if (nextChars.containsKey(nextChar)) {
					// increment the entry
					Integer value = nextChars.get(nextChar);
					nextChars.put(nextChar, new Integer(value + 1));
				} else {
					// create a new entry
					nextChars.put(nextChar, new Integer(1));
				}
			} else {
				// new hashtable containing this one entry
				nextChars = new HashMap<String, Integer>();
				nextChars.put(nextChar, new Integer(1));
				analysis.put(key, nextChars);
			}
		}
		return true; // currently, nothing can go wrong...
	}

	/**
	 * Returns the analysis in the form of a descriptive string
	 * 
	 * @return The description
	 */
	public String getAnalysisString() {
		TreeSet<String> sortedKeyStrings = new TreeSet<String>();

		// Add strings describing the keys to the TreeSet
		for (String key : analysis.keySet()) {
			HashMap<String, Integer> nextChars = analysis.get(key);
			StringBuffer thisKey = new StringBuffer(key + ':' + '\u01c1');
			for (String nextChar : nextChars.keySet()) {
				Integer value = nextChars.get(nextChar);
				thisKey.append(nextChar + "(" + value + ')' + '\u01c1');
			}
			String thisKeyString = thisKey.toString();
			thisKeyString = thisKeyString.replace('\n', '\u01c2');
			thisKeyString = thisKeyString.replace(' ', '_');
			thisKeyString = thisKeyString.replace('\u01c1', ' ');
			sortedKeyStrings.add(thisKeyString);
		}

		// Extract the strings - now sorted
		StringBuffer analysisBuffer = new StringBuffer();
		analysisBuffer.append("BOF: " + BOF + '\n');
		analysisBuffer.append("EOF: " + EOF + '\n');
		analysisBuffer.append("Newline: " + '\u01c2' + '\n');
		analysisBuffer.append("Space: " + '_' + "\n\n");
		for (String keyString : sortedKeyStrings) {
			analysisBuffer.append(keyString + '\n');
		}
		return analysisBuffer.toString();
	}

	/**
	 * Generate a "babble" output text
	 * 
	 * @return The generated text
	 */
	public String babble() {
		StringBuffer outputText = new StringBuffer();
		outputText.append(firstKey);
		String eofString = Character.toString(EOF);
		String key = firstKey;
		String nextChar;
		do {
			nextChar = babbleNextChar(key);
			outputText.append(nextChar);
			key = key.substring(1) + nextChar;
		} while (!nextChar.equals(eofString));
		String outputString = outputText.toString();
		// Drop BOF and EOF characters
		outputString = outputString.substring(1, outputString.length() - 2);
		return outputString;
	}

	/**
	 * Generate the character that should follow the given key. This is
	 * based on the analysis, which must have already been carried out.
	 * 
	 * @param key The key, representing the last characters generated
	 * @return The next character generated
	 */
	private String babbleNextChar(String key) {
		HashMap<String, Integer> nextChars = analysis.get(key);
		String out = null;

		// Count the total number of possibilities
		int totalEntries = 0;
		for (String c : nextChars.keySet()) {
			totalEntries += nextChars.get(c);
		}

		// Generate random number
		int entryWanted = rand.nextInt(totalEntries);

		// Find the entry wanted
		totalEntries = 0;
		for (String c : nextChars.keySet()) {
			totalEntries += nextChars.get(c);
			if (totalEntries > entryWanted) {
				out = c;
				break;
			}
		}
		return out;
	}
}
