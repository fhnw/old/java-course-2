package ch.fhnw.richards.InputOutputStreams;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;
import java.io.*;

public class BinaryFileCopy extends JFrame {
	// GUI components
	private JLabel jLabel1;
	private JTextField txtFileIn;
	private JLabel jLabel2;
	private JTextField txtFileOut;
	private JButton btnCopy;
	private JButton btnClose;

	public static void main(String args[]) {
		new BinaryFileCopy();
	}

	public BinaryFileCopy() {
		this.setTitle("Binary file copy");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		Container pane = getContentPane();
		pane.setLayout(new GridLayout(3, 2));

		jLabel1 = new JLabel("Input File");
		pane.add(jLabel1);
		txtFileIn = new JTextField();
		txtFileIn.setPreferredSize(new Dimension(150, 0));
		pane.add(txtFileIn);

		jLabel2 = new JLabel("Output File");
		pane.add(jLabel2);
		txtFileOut = new JTextField();
		pane.add(txtFileOut);

		// Copy button
		btnCopy = new JButton("Copy");
		btnCopy.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				copyFile();
			}
		});
		pane.add(btnCopy);

		// Quit button
		btnClose = new JButton("Quit");
		btnClose.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				endProgram();
			}
		});
		pane.add(btnClose);

		this.pack();
		this.setVisible(true);
	}

	private void copyFile() {
		String fileNameIn = txtFileIn.getText();
		String fileNameOut = txtFileOut.getText();

		// Create two file objects
		File fileIn = new File(fileNameIn);
		File fileOut = new File(fileNameOut);

		// Check the input file
		if (!fileIn.exists()) { // does not exist
			JOptionPane.showMessageDialog(this, "Input file does not exist.", "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (!fileIn.isFile()) { // is a directory
			JOptionPane.showMessageDialog(this, "Input file is a directory.", "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}
		if (!fileIn.canRead()) { // Not readable
			JOptionPane.showMessageDialog(this, "Input file cannot be read.", "Error",
					JOptionPane.ERROR_MESSAGE);
			return;
		}

		// Check the output file
		if (fileOut.exists()) { // exists - must be writeable
			if (!fileOut.canWrite()) {
				JOptionPane.showMessageDialog(this, "Output file is not writeable.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
		} else {
			String directoryName = fileOut.getParent();
			if (directoryName == null) { // No path, so use the current directory
				directoryName = System.getProperty("user.dir");
			}
			File directory = new File(directoryName);
			if (!directory.exists()) { // does not exist
				JOptionPane.showMessageDialog(this, "Output directory does not exist.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
			if (!directory.canWrite()) { // Not writeable
				JOptionPane.showMessageDialog(this, "Output directory is not writeable.", "Error",
						JOptionPane.ERROR_MESSAGE);
				return;
			}
		}

		// Finally, everything is ok!
		FileInputStream streamIn = null;
		FileOutputStream streamOut = null;
		try {
			// Create our input and output streams
			streamIn = new FileInputStream(fileIn);
			streamOut = new FileOutputStream(fileOut);
			byte[] buffer = new byte[4096];
			int bytesRead;

			// In a loop: read, write, read, write - until done
			bytesRead = streamIn.read(buffer); // the Read method returns the number of bytes
			while (bytesRead != -1) {
				streamOut.write(buffer, 0, bytesRead); // buffer, begin, length
				bytesRead = streamIn.read(buffer);
			}
			streamOut.flush(); // Ensure all data has actually been written
		} catch (Exception e) {
			JOptionPane.showMessageDialog(this, e.toString(), "Exception", JOptionPane.ERROR_MESSAGE);
		} finally { // Whether or not there was an error, we must close the streams
			if (streamIn != null) try {
				streamIn.close();
			} catch (IOException e2) {
			}
			if (streamOut != null) try {
				streamOut.close();
			} catch (IOException e3) {
			}
		}
	}

	private void endProgram() {
		System.exit(0);
	}
}
