package ch.fhnw.richards.helloWorld;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class HelloWorld2 extends JFrame {

	String[] args;

	// GUI Controls: Text box and label
	private JLabel lblHelloWorld;
	private JTextArea txtHelloWorld;

	public static void main(String[] args) {
		HelloWorld2 myProgram = new HelloWorld2(args);
		myProgram.startGUI();
	}

	public HelloWorld2(String[] args) {
		super("Hello, World!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		// All setup except for the GUI
		this.args = args;
		
		// We could also call "startGUI" here, instead of in "main"
	}
	
	private void startGUI() {
		// Create and initialize controls
		lblHelloWorld = new JLabel("Command line args are listed below:");

		txtHelloWorld = new JTextArea();
		for (String a : this.args) {
			txtHelloWorld.append(a + "\n");
		}

		// Place controls in Layout manager
		this.setLayout(new BorderLayout());
		this.add(lblHelloWorld, BorderLayout.NORTH);
		this.add(txtHelloWorld, BorderLayout.CENTER);

		// Pack and show
		this.pack();
		this.setVisible(true);
	}
}
