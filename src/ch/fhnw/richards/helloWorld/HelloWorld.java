package ch.fhnw.richards.helloWorld;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextArea;

public class HelloWorld extends JFrame {
	String[] args;

	// GUI Controls: Text box and label
	private JLabel lblHelloWorld;
	private JTextArea txtHelloWorld;

	public static void main(String[] args) {
		new HelloWorld(args);
	}

	public HelloWorld(String[] args) {
		super("Hello, World!"); // Super-const. sets frame title
		this.args = args;
		// Set layout manager
		this.setLayout(new BorderLayout());
		// Create controls
		lblHelloWorld = new JLabel("Command line args are listed below:");
		this.add(lblHelloWorld, BorderLayout.NORTH);
		txtHelloWorld = new JTextArea();
		this.add(txtHelloWorld, BorderLayout.CENTER);
		// Fill text area with stuff
		for (String a : this.args) {
			txtHelloWorld.append(a + "\n");
		}
		// Dynamic layout
		this.pack();
		// Show result
		this.setVisible(true);
	}
}
