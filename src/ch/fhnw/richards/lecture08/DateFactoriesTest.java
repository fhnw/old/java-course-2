/**
 * 
 */
package ch.fhnw.richards.lecture08;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.Collection;
import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

/**
 * @author brad
 * 
 */
@RunWith(Parameterized.class)
public class DateFactoriesTest {
	private int day, month, year;
	private boolean goodDate;
	public Date expectedDate;

	public DateFactoriesTest(int day, int month, int year, boolean goodDate) {
		this.year = year;
		this.month = month;
		this.day = day;
		this.goodDate = goodDate;
	}
	
	@Before
	public void setExpectedDate() {
		expectedDate = new Date(year-1900, month-1, day);
	}

	@Parameters
	public static Collection<Object[]> inputDates() {
		return Arrays.asList(new Object[][] {
				{ 30, 6, 2008, true },
				{ 29, 2, 2008, true },
				{ 29, 2, 2000, true },
				{ 29, 2, 1900, false } });
	}

	@Test
	public void testMakeDateGood() {
		if (goodDate) { // only test the dates that should be ok
			assertEquals(DateFactories.makeDate(day, month, year), expectedDate);
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void testMakeDateBad() {
		if (!goodDate) { // only test the dates that should be ok
			DateFactories.makeDate(day, month, year);
		} else {
			throw new IllegalArgumentException();
		}
	}
}
