package ch.fhnw.richards.lecture08;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Date;

public class DateFactories {
  public static Date makeDate(int day, int month, int year) {
  	GregorianCalendar cal = new GregorianCalendar(); // month is 0-based
  	cal.setTimeInMillis(0);
  	cal.set(year, month-1, day,0,0,0);
  	cal.setLenient(false); // do not accept invalid values
    return cal.getTime();
  }
}
