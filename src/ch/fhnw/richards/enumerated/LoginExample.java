package ch.fhnw.richards.enumerated;

import java.util.ArrayList;

public class LoginExample {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// Set up a few users from our supposed database
		ArrayList<String> normalUsers = new ArrayList<String>();
		normalUsers.add("Mary Smith");
		normalUsers.add("James Thompson");
		normalUsers.add("Sam Miller");
		ArrayList<String> advancedUsers = new ArrayList<String>();
		advancedUsers.add("Ralf Jones");
		advancedUsers.add("Jane Schmid");
		ArrayList<String> adminUsers = new ArrayList<String>();
		adminUsers.add("Anne Peterson");
		
		// Here is the user who just logged in
		User currentUser = new User("Ralf Jones");
		
		// Should this person have normal privileges?
		if (normalUsers.contains(currentUser.getUserName())) {
			currentUser.raisePrivileges(Login.NORMAL);
		}
		
		// Should this person have advanced privileges?
		if (advancedUsers.contains(currentUser.getUserName())) {
			currentUser.raisePrivileges(Login.ADVANCED);
		}

		System.out.println("Current privilege level is " + currentUser.getCurrentPrivileges());
	}
}

enum Login { GUEST, NORMAL, ADVANCED, ADMIN }

class User {
	private String userName;
	Login currentPrivileges;
	
	public User(String userName) {
		this.userName = userName;
		currentPrivileges = Login.GUEST;
	}

	public String getUserName() {
		return userName;
	}
	
	public Login getCurrentPrivileges() {
		return currentPrivileges;
	}
	
	public void raisePrivileges(Login newPriv) {
		if (newPriv.compareTo(currentPrivileges) > 0) {
			currentPrivileges = newPriv;
		}
	}
}