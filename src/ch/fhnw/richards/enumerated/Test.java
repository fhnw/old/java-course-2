package ch.fhnw.richards.enumerated;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.Map;

public class Test {

	public static void main(String[] args) {
		System.out.println(proportionOfEarth(Planet.PLUTO));
		printAllPlanets();
		System.out.println(Planet.MERCURY.weather());
	}

	public static boolean comesBefore(Planet x, Planet y) {
		boolean result;
		result = (x.compareTo(y) < 0);
		return result;
	}

	public static double proportionOfEarth(Planet p) {
		double earthGravity = Planet.EARTH.surfaceGravity();
		double pGravity = p.surfaceGravity();
		return pGravity / earthGravity;
	}

	public static void printAllPlanets() {
		 for ( Planet p : EnumSet.range(Planet.MERCURY, Planet.PLUTO)) {
		 System.out.println(p);
		 }
		

//		EnumSet<Planet> visited = EnumSet.of(Planet.VENUS, Planet.EARTH,
//				Planet.MARS);
//		visited.add(Planet.PLUTO);
//		visited.add(Planet.EARTH); // will not be added - already present
//		for (Planet p : visited) {
//			System.out.println(p);
//		}

		
//		Map<Planet, ArrayList<String>> visitorLists = new EnumMap<Planet, ArrayList<String>>(Planet.class);
//		for (Planet p : Planet.values()) {
//			ArrayList<String> visitors = new ArrayList<String>();
//			visitorLists.put(p, visitors);
//		}
//		
//		// Get visitor list for Mars, and add someone to it
//		ArrayList<String> marsVisitors = visitorLists.get(Planet.MARS);
//		marsVisitors.add("Ralph");
	}
}
