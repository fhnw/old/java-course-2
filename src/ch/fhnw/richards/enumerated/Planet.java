package ch.fhnw.richards.enumerated;

public enum Planet {
  MERCURY (3.303e+23, 2.4397e6) {
  	public String weather() { return "too hot"; }
  },
  VENUS (4.869e+24, 6.0518e6),
  EARTH (5.976e+24, 6.37814e6),
  MARS (6.421e+23, 3.3972e6),
  PLUTO (1.27e+22, 1.137e6);
  private final double mass; // in kilograms
  private final double radius; // in meters
  Planet(double mass, double radius) {
    this.mass = mass;
    this.radius = radius;
  }
  
  // universal gravitational constant (m3 kg-1 s-2)
  public static final double G = 6.67300E-11;
  private double mass() {
    return mass;
  }
  private double radius() {
    return radius;
  }
  double surfaceGravity() {
    return G * mass / (radius * radius);
  }
  double surfaceWeight(double otherMass) {
    return otherMass * surfaceGravity();
  }
  
  public String weather() {
  	return "unknown";
  }
}
