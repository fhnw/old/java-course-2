package ch.fhnw.richards.swing_memory_leak;

import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Date;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JButton;

/**
 * A demostration of memory leaks that occur when you fail to remove a listener.
 */
public class FixedLeakDemo extends JFrame implements Runnable {

	private JPanel canvas = new JPanel();
	private JPanel buttonPanel = new JPanel();
	private JButton launchButton = new JButton("Launch");
	private JButton fasterButton = new JButton("Faster");
	private JButton slowerButton = new JButton("Slower");

	/**
	 * A thread to do the rendering on, since we don't want to render on the user interface thread.
	 */
	Thread renderThread = new Thread(this);

	/**
	 * Stores all the ball threads.
	 */
	List balls = Collections.synchronizedList(new ArrayList());

	/**
	 * Constructs and lays out the application frame.
	 */
	public FixedLeakDemo() {
		launchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for (int i = 0; i < 100; i++) {
					Ball ball = new Ball();
					balls.add(ball);
					ball.start();
				}
			}
		});
		buttonPanel.add(launchButton);
		buttonPanel.add(fasterButton);
		buttonPanel.add(slowerButton);
		getContentPane().add(buttonPanel, "South");
		getContentPane().add(canvas, "Center");
		setResizable(false);
	}

	/**
	 * Renders the canvas with all the balls. First it creates a back buffer and graphics objects for
	 * the back buffer and the canvas. Then it loops around drawing all the live balls to the back
	 * buffer then blitting the back buffer to the canvas.
	 */
	public void run() {
		Image buffer = createImage(canvas.getWidth(), canvas.getHeight());
		Graphics g = buffer.getGraphics();
		Graphics gc = canvas.getGraphics();
		while (true) {
			g.setColor(Color.white);
			g.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());
			synchronized (balls) {
				for (Iterator i = balls.iterator(); i.hasNext();) {
					Ball b = (Ball) i.next();
					b.draw(g);
				}
			}
			gc.drawImage(buffer, 0, 0, canvas);
			try {
				System.gc();
				Thread.sleep(5);
			} catch (InterruptedException e) {
			}
		}
	}

	/**
	 * Runs the demonstration as an application.
	 */
	public static void main(String[] args) {
		FixedLeakDemo bouncer = new FixedLeakDemo();
		bouncer.setTitle("Bouncing Balls");
		bouncer.setSize(400, 300);
		bouncer.show();
		bouncer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		bouncer.renderThread.setPriority(Thread.MAX_PRIORITY);
		bouncer.renderThread.start();
	}

	/**
	 * A ball is something that bounces around on a JPanel.
	 */
	class Ball extends Thread implements Cloneable {
		private Color color = Color.black;
		private int x = 0;
		private int y = 0;
		private int dx = 2;
		private int dy = 2;
		private int delay = 10;
		private static final int RADIUS = 10;
		private long[] dataArray = new long[1000000];

		ActionListener slowerButtonListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				delay++;
			}
		};

		ActionListener fasterButtonListener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (delay > 5)
					delay--;
			}
		};

		public Ball() {
			for (int i = 0; i < dataArray.length; i++) {
				dataArray[i] = (long) (Math.random() * 1000.0);
			}

			slowerButton.addActionListener(slowerButtonListener);
			fasterButton.addActionListener(fasterButtonListener);
		}

		/**
		 * Draws the ball as a filled in circle.
		 */
		public void draw(Graphics g) {
			g.setColor(color);
			g.fillOval(x, y, RADIUS, RADIUS);
		}

		/**
		 * Updates the position of the ball, taking care of bounces.
		 */
		protected void move() {
			x += dx;
			y += dy;
			if (x < 0) {
				// Bounce off left wall.
				x = 0;
				dx = -dx;
			}
			if (x + RADIUS >= canvas.getWidth()) {
				// Bounce off right wall.
				x = canvas.getWidth() - RADIUS;
				dx = -dx;
			}
			if (y < 0) {
				// Bounce off top wall.
				y = 0;
				dy = -dy;
			}
			if (y + RADIUS >= canvas.getHeight()) {
				// Bounce off bottom wall.
				y = canvas.getHeight() - RADIUS;
				dy = -dy;
			}
		}

		/**
		 * A ball basically just moves 1000 times. After each position update, it sleeps for a few
		 * milliseconds.
		 */
		public void run() {
			for (int i = 1; i <= 200; i++) {
				move();
				try {
					Thread.sleep(delay);
				} catch (InterruptedException e) {
				}
			}
			balls.remove(this);
			fasterButton.removeActionListener(fasterButtonListener);
			slowerButton.removeActionListener(slowerButtonListener);
		}

	}
}