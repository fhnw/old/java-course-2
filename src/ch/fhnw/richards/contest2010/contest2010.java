package ch.fhnw.richards.contest2010;

import java.util.Date;

public class contest2010 {

	/**
	 * Studienganginformatik Wettbewerb 2010
	 */
	public static void main(String[] args) {
		long timeStart = (new Date()).getTime();
		final int numNums = 10;
		final int startValueToTry = 50;
		final int maxValueToTry = 50;
		int nums[] = new int[numNums];

		int largest_g = -1;
		for (int maxValue = startValueToTry; maxValue <= maxValueToTry; maxValue++) {
			System.out.println();
			System.out.println("MaxValue = " + maxValue);

			// init
			for (int i = 0; i < numNums; i++) {
				nums[i] = i;
			}

			boolean done = false;
			while (!done) {
				int pos = numNums - 1;
				boolean incremented = false;
				while (!incremented & pos >= 0) {
					if (nums[pos] < (maxValue - (numNums - pos - 1))) {
						nums[pos]++;
						for (int i = pos + 1; i < numNums; i++) {
							nums[i] = nums[pos];
						}
						incremented = true;
					} else {
						pos--;
					}
				}
				if (incremented) {
					int g = tryNumbers(nums);
					if (g >= largest_g) {
						largest_g = g;
						System.out.print("[");
						for (int i = 0; i < numNums; i++)
							System.out.print(nums[i] + " ");
						System.out.println("] g = " + g);
					}
				} else {
					done = true;
				}
			}
		}
		long timeEnd = (new Date()).getTime();
		long milliseconds = timeEnd - timeStart;
		int seconds = (int) (milliseconds / 1000);
		milliseconds = milliseconds - 1000 * seconds;
		int minutes = seconds / 60;
		seconds = seconds - 60 * minutes;
		System.out.println(minutes + ":" + seconds + "." + milliseconds);
	}

	/**
	 * Test a set of 10 natural numbers, to see which natural numbers in the range
	 * 0-199 we can generate. Return "g", which is the value of the last number
	 * generated before the first gap. In other words, if we are able to generate
	 * the numbers 0, 1, 2, 3, 5, 6, 8, 9 then we return "3".
	 * 
	 * Note that generating 0 and 1 is trivial (using subtraction and division),
	 * so we do not bother testing these two numbers.
	 * 
	 * We assume that "nums" is sorted from smallest to largest.
	 */
	public static int tryNumbers(int[] nums) {
		// boolean array for the first natural numbers; these are used to calculate
		// "g"
		final int numToTest = 200;
		boolean numsFound[] = new boolean[numToTest];

		int limit = nums.length;

		// init
		for (int i = 0; i < numToTest; i++)
			numsFound[i] = false;

		// Generate all possible natural numbers
		for (int i = 0; i < limit; i++) {
			for (int j = i; j < limit; j++) {
				// Addition
				int sum = nums[i] + nums[j];
				if (sum < 200)
					numsFound[sum] = true;

				// Subtraction: we only subtract smaller from larger
				int diff = nums[j] + nums[i];
				if (diff < 200)
					numsFound[diff] = true;

				// Multiplication
				int prod = nums[j] * nums[i];
				if (prod < 200)
					numsFound[prod] = true;

				// Division: we only divide larger by smaller
				if (nums[i] > 0) {
					int quot = nums[j] / nums[i];
					if (quot < 200)
						numsFound[quot] = true;
				}
			}
		}

		// Find the first gap
		int g = -1;
		for (int i = 2; i < numToTest & g < 0; i++) {
			if (!numsFound[i]) {
				g = i - 1;
			}
		}

		return g;
	}
}
