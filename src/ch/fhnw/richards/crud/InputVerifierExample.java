package ch.fhnw.richards.crud;

import java.awt.Color;
import java.awt.DefaultKeyboardFocusManager;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.KeyboardFocusManager;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JTextField;

public class InputVerifierExample extends JFrame {
	static JTextField txtOne = new JTextField("");
	static JTextField txtTwo = new JTextField("This field may not be empty");
	static KeyboardFocusManager kfm = new DefaultKeyboardFocusManager();

	public static void main(String[] args) {
		new InputVerifierExample();
	}

	private InputVerifierExample() {
		super("Input Verifier Example");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(300, 70));
		setLayout(new GridLayout(2, 1));
		InputVerifier validator = new NotEmpty();
		txtOne.setInputVerifier(validator);
		txtTwo.setInputVerifier(validator);
		add(txtOne);
		add(txtTwo);
		pack();
		setVisible(true);
	}

	private class NotEmpty extends InputVerifier {
		@Override
		public boolean verify(JComponent input) {
			JTextField txtField = (JTextField) input;
			boolean isNotEmpty = (txtField.getText().length() > 0);
			if (isNotEmpty) {
				txtField.setBackground(Color.WHITE);
			} else {
				txtField.setBackground(Color.PINK);
			}
			return isNotEmpty;
		}
	}
}
