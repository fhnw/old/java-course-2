package ch.fhnw.richards.lecture04.v3;

import java.awt.Cursor;

import javax.swing.Icon;

public interface DrawingToolInterface {
	public void setTool(Icon icon);
	public Icon getTool();
	public void setCursor(Cursor cursor);
}
