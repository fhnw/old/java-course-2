package ch.fhnw.richards.lecture04.v2;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class Canvas extends java.awt.Canvas implements MouseListener {
	private Paint parent;
	private Point mousePress = null;
	private ArrayList<Line> lines = new ArrayList<Line>();

	public Canvas(Paint parent) {
		super();
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(300, 300));
		this.parent = parent;
		this.addMouseListener(this);
	}

	@Override
	public void paint(Graphics g) {
		for (Line line : lines) {
			g.setColor(line.color);
			g.drawLine(line.start.x, line.start.y, line.end.x, line.end.y);
		}
	}

	@Override
	public void mousePressed(MouseEvent evt) {
		mousePress = evt.getPoint();
	}

	@Override
	public void mouseReleased(MouseEvent evt) {
		Point mouseUp = evt.getPoint();
		Color color = parent.getColor();
		if (mousePress != null) {
			lines.add(new Line(mousePress, mouseUp, color));
			Graphics g = this.getGraphics();
			if (g != null) { // should never happen, but...
				g.setColor(color);
				g.drawLine(mousePress.x, mousePress.y, mouseUp.x, mouseUp.y);
			}
		}
	}

	@Override
	public void mouseClicked(MouseEvent evt) {
	}

	@Override
	public void mouseExited(MouseEvent evt) {
	}

	@Override
	public void mouseEntered(MouseEvent evt) {
	}

	private class Line {
		private Point start;
		private Point end;
		private Color color;

		private Line(Point start, Point end, Color color) {
			this.start = start;
			this.end = end;
			this.color = color;
		}
	}
}
