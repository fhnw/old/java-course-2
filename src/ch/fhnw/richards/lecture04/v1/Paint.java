package ch.fhnw.richards.lecture04.v1;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;

import javax.swing.Box;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

public class Paint extends JFrame implements ColorPotInterface {
	private Canvas canvas;
	private Box tools;
	private Color currentColor;
	
	public static void main(String[] args) {
		new Paint();
	}

	private Paint() {
		Container pane = this.getContentPane();
		pane.setLayout(new BorderLayout());

		tools = Box.createVerticalBox();
		tools.add(new ColorPot(this, Color.RED));
		tools.add(new ColorPot(this, Color.BLUE));
		tools.add(new ColorPot(this, Color.GREEN));
		tools.add(new ColorPot(this, Color.WHITE));
		tools.add(new ColorPot(this, Color.BLACK));
		tools.add(Box.createVerticalGlue());
		pane.add(tools, BorderLayout.WEST);

		canvas = new Canvas(this);
		pane.add(canvas, BorderLayout.CENTER);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
    // set defaults
    currentColor = Color.black;
    
		pack();
		setVisible(true);		
	}

	@Override
	public Color getColor() {
		return currentColor;
	}

	@Override
	public void setColor(Color color) {
		currentColor = color;
	}
}
