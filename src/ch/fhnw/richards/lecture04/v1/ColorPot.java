package ch.fhnw.richards.lecture04.v1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class ColorPot extends JButton implements ActionListener {
	ColorPotInterface parent;
	public ColorPot(ColorPotInterface parent, Color color) {
		super();
		this.parent = parent;
		this.setPreferredSize(new Dimension(20, 20));
		this.setBackground(color);
		this.addActionListener(this);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		parent.setColor(this.getBackground());
	}

}
