package ch.fhnw.richards.lecture04.v1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;

public class Canvas extends java.awt.Canvas implements MouseListener {
	private Paint parent;
	private Point mousePress = null;
	
	public Canvas(Paint parent) {
		super();
		this.setBackground(Color.WHITE);
		this.setPreferredSize(new Dimension(300, 300));
		this.parent = parent;
		this.addMouseListener(this);
	}
	
	@Override
	public void mousePressed(MouseEvent evt) {
		mousePress = evt.getPoint();
	}
	@Override
	public void mouseReleased(MouseEvent evt) {
    Point mouseUp = evt.getPoint();
    if (mousePress != null) {
      Graphics g = this.getGraphics();
     if (g != null) { // should never happen, but...
        g.setColor(parent.getColor());
        g.drawLine(mousePress.x, mousePress.y, mouseUp.x, mouseUp.y);
      }
    }
	}
	@Override
	public void mouseClicked(MouseEvent evt) {
	}
	@Override
	public void mouseExited(MouseEvent evt) {
	}
	@Override
	public void mouseEntered(MouseEvent evt) {
	}
}
