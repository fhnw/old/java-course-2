package ch.fhnw.richards.lecture04.v1;

import java.awt.Color;

public interface ColorPotInterface {
	public Color getColor();
	public void setColor(Color color);
}
