package ch.fhnw.richards.threads.whacamole;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;

public class MoleButton extends JButton implements Runnable, ActionListener {
	private Whacamole parent;
	Thread t;
	
	MoleButton(Whacamole parent) {
		super();
		this.parent = parent;
		this.setIcon(parent.moleIcon);
		this.setDisabledIcon(parent.emptyIcon);
		this.setPreferredSize(new Dimension(60,60));
		this.setEnabled(false);
		
		this.addActionListener(this);
		
		t = new Thread(this);
		t.start();
	}
	
	@Override
	public void run() {
		while(true) {
			this.setEnabled(Math.random() < 0.3);
			try {
				Thread.sleep(300);
			} catch (InterruptedException e) {
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		parent.whack();
	}

}
