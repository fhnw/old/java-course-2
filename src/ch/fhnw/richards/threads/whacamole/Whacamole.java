package ch.fhnw.richards.threads.whacamole;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class Whacamole extends JFrame {
	private int score = -1;
	private JLabel lblScore = new JLabel();
	final Icon moleIcon;
	final Icon emptyIcon;

	public static void main(String[] args) {
		new Whacamole();
	}

	private Whacamole() {
		super("Whac-A-Mole");
		emptyIcon = createImageIcon(this.getClass(), "empty.gif");
		moleIcon = createImageIcon(this.getClass(), "mole.gif");
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());
		this.add(lblScore, BorderLayout.NORTH);

		JPanel molePanel = new JPanel();
		molePanel.setBorder(new EmptyBorder(10,10,10,10));
		this.add(molePanel, BorderLayout.CENTER);
		molePanel.setLayout(new GridLayout(3, 3, 10, 10));
		for (int i = 1; i <= 9; i++) {
			MoleButton mole = new MoleButton(this);
			molePanel.add(mole);
		}
		whack(); // Initialize the score
		pack();
		setVisible(true);
	}

	void whack() {
		score++;
		lblScore.setText("Score: " + score + " moles whacked!");
	}

	/**
	 * Returns an ImageIcon, or null if the path was invalid. (adapted from
	 * http://java.sun.com/docs/books/tutorial/uiswing/misc/icon.html)
	 * 
	 * @param callingClass
	 *            - the class associated with the image we are to convert to an
	 *            icon
	 * @param path
	 *            - the path to the image, relative to the class #param
	 *            description - description of the icon (optional)
	 */
	public static javax.swing.ImageIcon createImageIcon(Class<?> callingClass,
			String path, String... description) {
		java.net.URL imgURL = callingClass.getResource(path);
		if (imgURL != null) {
			if (description.length > 0)
				return new javax.swing.ImageIcon(imgURL, description[0]);
			else
				return new javax.swing.ImageIcon(imgURL);
		} else {
			System.err.println("Couldn't find resource: " + path);
			return null;
		}
	}
}
