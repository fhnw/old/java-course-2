package ch.fhnw.richards.threads.animation;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JFrame;
import javax.swing.JPanel;

@SuppressWarnings("serial")
public class AnimationExample extends JFrame implements Runnable {
	public static final int SIZE = 400;
	private final int frameRate = 30;
	private ColoredWheel wheel; // Animated object
	private MyCanvas canvas; // Drawing area

	public static void main(String[] args) {
		new AnimationExample();
	}

	public AnimationExample() {
		super("Animation example");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());

		// Create the animated object, and start its animation
		wheel = new ColoredWheel(SIZE, SIZE);
		wheel.start();

		// Create the canvas to draw on
		canvas = new MyCanvas(wheel);
		canvas.setPreferredSize(new Dimension(SIZE, SIZE));
		this.add(canvas, BorderLayout.CENTER);

		// Create and start the display refresh thread
		Thread displayThread = new Thread(this, "Display thread");
		displayThread.start();

		// Show our window
		this.pack();
		this.setVisible(true);
	}

	@Override
	public void run() {
		while(true) {
			// Tell Swing to call canvas.paint() at the next opportunity
			canvas.repaint();
	
			// Sleep for a while; maintain the frame-rate we set
			try {
				Thread.sleep(1000 / frameRate);
			} catch (InterruptedException e) {
			}
		}
	}
	
	private static class MyCanvas extends JPanel {
		// Buffer and graphics object for double-buffering
		private BufferedImage buff = new BufferedImage(SIZE, SIZE, BufferedImage.TYPE_INT_RGB);
		private Graphics gBuff = buff.createGraphics();
		private ColoredWheel wheel;
		
		public MyCanvas(ColoredWheel wheel) {
			this.wheel = wheel;
		}
		
		@Override
		public void paint(Graphics g) {
			// Erase our buffer
			gBuff.setColor(Color.WHITE);
			gBuff.fillRect(0, 0, SIZE, SIZE);

			// Tell the object to display itself in our buffer
			wheel.paint(gBuff);

			// Repaint the display
		    g.drawImage(buff, 0, 0, buff.getWidth(), buff.getHeight(), this);
		}
	}	
}
