package ch.fhnw.richards.threads.animation;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import javax.swing.JApplet;

public class WheelAppletBuffered extends JApplet implements Runnable {
	private final int frameRate = 30;
	private ColoredWheel wheel;
	private Thread displayThread;
	private volatile boolean stopThread = false;
	private BufferedImage buff; // Buffer for off-screen image
	private Graphics gBuff; // Graphics object for buffer
	
	@Override
	public void init() {
		// Create the buffer and graphics object
		buff = new BufferedImage(400,400, BufferedImage.TYPE_INT_RGB);
		gBuff = buff.createGraphics();

		wheel = new ColoredWheel(400,400);
		displayThread = new Thread(this);
		wheel.start();
		displayThread.start();
	}
	
	@Override
	public void destroy() {
		wheel.stopThread();
		wheel.interrupt();
		stopThread = true;
	}
	
	@Override
	public void run() {
		while (!stopThread) {
			this.repaint();			
			try {
				Thread.sleep(1000 / frameRate);
			} catch (InterruptedException e) {
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		gBuff.setColor(Color.WHITE);
		gBuff.fillRect(0,0,400,400);
		wheel.paint(gBuff);
		super.paint(g);
		g.drawImage(buff, 0, 0, buff.getWidth(), buff.getHeight(), this);
	}
}
