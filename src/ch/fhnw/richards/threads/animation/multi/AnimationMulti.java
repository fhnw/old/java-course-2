package ch.fhnw.richards.threads.animation.multi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.net.URL;
import java.util.ArrayList;

public class AnimationMulti extends javax.swing.JApplet implements Runnable {
	private BufferedImage buff; // Buffer for off-screen image
	private Graphics gBuff; // Graphics object for buffer
	private int frameRate;
	public final int yMax = 400;
	protected final int xMax = 400;
	private Thread displayThread;
	private volatile boolean stopThread; // a variable indicating that the thread should stop.
	// "volatile" means that this variable is accessed from
	// multiple threads, and may not be cached by the JVM

	private ArrayList<AnimObject> things = new ArrayList<AnimObject>();

	@Override
	public void init() {
		setBackground(Color.WHITE);

		
		// Create the buffer and graphics object
		buff = new BufferedImage(401, 401, BufferedImage.TYPE_INT_RGB);
		gBuff = buff.createGraphics();
		frameRate = 30;

		// Set up the display thread
		stopThread = false;
		displayThread = new Thread(this);
		displayThread.setName("BB-repaint");
		displayThread.start();

		// Add some circles
		for (int i = 0; i < 3; i++) {
			Color objColor;
			switch (i % 3) {
			case 0:
				objColor = Color.blue;
				break;
			case 1:
				objColor = Color.green;
				break;
			case 2:
				objColor = Color.red;
				break;
			default:
				objColor = Color.black;
			}
			Point position = new Point(i * 10, i * 20);
			Point velocity = new Point(i + 1, i + 2);
			things.add(new AnimCircle(this, "ball-" + i, objColor, position, velocity, 10));
		}

		// Add an image
		URL url = getClass().getResource("ship.gif");
		Image animationImage = Toolkit.getDefaultToolkit().getImage(url);
		Point position = new Point(50, 50);
		Point velocity = new Point(4, 2);
		things
				.add(new AnimImage(this, "ship.gif", Color.black, position, velocity, animationImage));

	}

	@Override
	public void destroy() {
		for (AnimObject thing : things) {
			thing.stop();
		}
		stopThread = true;
	}

	@Override
	public void paint(Graphics pGraphics) {
		gBuff.setColor(Color.WHITE);
		gBuff.fillRect(0, 0, 401, 401);
		for (AnimObject thing : things) {
			thing.paint(gBuff);
		}
		pGraphics.drawImage(buff, 0, 0, buff.getWidth(), buff.getHeight(), this);
	}

	@Override
	public void run() {
		while (!stopThread) { // continue until we are told to stop
			repaint();

			try {
				Thread.sleep(1000 / frameRate);
			} catch (InterruptedException e) {
			}
		}
		displayThread = null; // dereference the thread - not really necessary, but a nicety
	}
}
