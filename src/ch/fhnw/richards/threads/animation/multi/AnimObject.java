package ch.fhnw.richards.threads.animation.multi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

abstract class AnimObject implements Runnable {
	protected String name;
	protected Color color;
	protected Point position;
	protected Point velocity;
	protected AnimationMulti parent;

	protected Thread objectThread;
	protected volatile boolean running;

	AnimObject(AnimationMulti parent, String name, Color color, Point position, Point velocity) {
		this.parent = parent;
		this.name = name;
		this.color = color;
		this.position = position;
		this.velocity = velocity;
	}

	public void start() {
		running = true;

		// initialize the thread for this object
		objectThread = new Thread(this);
		objectThread.setName("Anim-" + this.name);
		objectThread.start();
	}

	public void stop() {
		running = false;
	}

	abstract public void run();

	abstract public void paint(Graphics pGraphics);
}
