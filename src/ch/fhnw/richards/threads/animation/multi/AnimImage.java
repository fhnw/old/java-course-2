package ch.fhnw.richards.threads.animation.multi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;

class AnimImage extends AnimObject implements Runnable {
	private Image image;

	public AnimImage(AnimationMulti parent, String name, Color color, Point position, Point velocity,
			Image image) {
		super(parent, name, color, position, velocity);
		this.image = image;

		super.start();
	}

	public void paint(Graphics g) {
		g.drawImage(image, position.x, position.y, parent);
		g.setColor(color);
		g.drawString(name, (int) (position.x + image.getWidth(parent) + 5), (int) (position.y + image.getHeight(parent)));
	}

	public void run() {
		while (running) { // continue until we are told to stop
			// vertical position
			if (velocity.y < 0) {
				if ((position.y + velocity.y) < 0) velocity.y = 0 - velocity.y;
			} else {
				if ((position.y + velocity.y + image.getHeight(parent)) > parent.yMax)
					velocity.y = 0 - velocity.y;
			}
			position.y += velocity.y;

			// horizontal position
			if (velocity.x < 0) {
				if ((position.x + velocity.x) < 0) velocity.x = 0 - velocity.x;
			} else {
				if ((position.x + velocity.x + image.getWidth(parent)) > parent.xMax)
					velocity.x = 0 - velocity.x;
			}
			position.x += velocity.x;

			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
		}
		objectThread = null; // dereference the thread - not really necessary, but a nicety
	}
}