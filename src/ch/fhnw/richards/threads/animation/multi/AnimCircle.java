package ch.fhnw.richards.threads.animation.multi;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

class AnimCircle extends AnimObject implements Runnable {
	private double radius;

	public AnimCircle(AnimationMulti parent, String name, Color color, Point position, Point velocity,
			int radius) { // perform all initialization for this object
		super(parent, name, color, position, velocity); // initialize the super-class
		this.radius = radius; // specific to "Circle"

		// after initialization, start the thread for this object
		super.start();
	}

	public void paint(Graphics g) {
		g.setColor(color);
		g.fillOval(position.x, position.y, (int) (2 * radius), (int) (2 * radius));
		g.drawString(name, (int) (position.x + 2 * radius + 5), (int) (position.y + 2 * radius));
	}

	public void run() {
		while (running) { // continue until we are told to stop
			// vertical position
			if (velocity.y < 0) {
				if ((position.y + velocity.y) < 0) velocity.y = 0 - velocity.y;
			} else {
				if ((position.y + velocity.y + 2 * radius) > parent.yMax) velocity.y = 0 - velocity.y;
			}
			position.y += velocity.y;

			// horizontal position
			if (velocity.x < 0) {
				if ((position.x + velocity.x) < 0) velocity.x = 0 - velocity.x;
			} else {
				if ((position.x + velocity.x + 2 * radius) > parent.xMax) velocity.x = 0 - velocity.x;
			}
			position.x += velocity.x;

			try {
				Thread.sleep(50);
			} catch (InterruptedException e) {
			}
		}
		objectThread = null; // dereference the thread - not really necessary, but a nicety
	}
}
