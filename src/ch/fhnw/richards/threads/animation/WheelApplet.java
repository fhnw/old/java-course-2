package ch.fhnw.richards.threads.animation;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JApplet;

public class WheelApplet extends JApplet implements Runnable {
	private final int frameRate = 30;
	private ColoredWheel wheel;
	private Thread displayThread;
	private volatile boolean stopThread = false;
	
	@Override
	public void init() {
		wheel = new ColoredWheel(400,400);
		displayThread = new Thread(this);
		wheel.start();
		displayThread.start();
	}

	@Override
	public void destroy() {
		wheel.stopThread();
		wheel.interrupt();
		stopThread = true;
	}
	@Override
	public void run() {
		while (!stopThread) {
			this.repaint();			
			try {
				Thread.sleep(1000 / frameRate);
			} catch (InterruptedException e) {
			}
		}
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		g.setColor(Color.WHITE);
		g.fillRect(0,0,400,400);
		wheel.paint(g);
	}
}
