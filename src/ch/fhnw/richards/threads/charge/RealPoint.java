package ch.fhnw.richards.threads.charge;

import java.awt.Point;

/**
 * Similar to the class Point, but maintains the location using
 * doubles, for more accuracy
 * 
 * @author brad
 *
 */
public class RealPoint {
	public double x;
	public double y;
	
	public RealPoint(double x, double y) {
		this.x = x;
		this.y = y;
	}
	
	public RealPoint(Point point) {
		this.x = point.x;
		this.y = point.y;
	}
	
	public RealPoint() {
		this.x = 0;
		this.y = 0;
	}
}
