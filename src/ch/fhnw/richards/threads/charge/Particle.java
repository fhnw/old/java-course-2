package ch.fhnw.richards.threads.charge;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

public class Particle extends Thread {
	// Force between particles varies by distance. In reality
	// quadratic, but we can play with other values
	public enum Power {
		LINEAR, QUADRATIC
	};

	public static Power power;
	public static double C = 0.1; // static attraction constant

	private Point screenPosition;
	private RealPoint position;
	private RealPoint velocity;
	private int charge;
	private volatile boolean stopThread;
	private ArrayList<Particle> particles;
	private Charge parent;

	public Particle(Point pos, int charge, ArrayList<Particle> particles, Charge parent, String name) {
		super(name);
		screenPosition = pos;
		position = new RealPoint(pos);
		velocity = new RealPoint(0, 0);
		this.charge = charge;
		this.particles = particles;
		this.parent = parent;
		stopThread = false;
	}

	@Override
	public void run() {
		while (!stopThread) {
			RealPoint acceleration = new RealPoint();
			// Calculate the force for each other particle and alter our
			// velocity accordingly: Acceleration = Force / Mass
			synchronized (particles) {
				for (Particle p : particles) {
					if (p != this) {
						calculateAccel(acceleration, p);
					}
				}
			}

			// Alter our position, both real and on the screen
			velocity.x += acceleration.x;
			velocity.y += acceleration.y;
			position.x += velocity.x;
			position.y += velocity.y;
			screenPosition.x = (int) position.x;
			screenPosition.y = (int) position.y;
			if (screenPosition.x < 0 | screenPosition.x > parent.getCanvasWidth()) stopThread = true;
			if (screenPosition.y < 0 | screenPosition.y > parent.getCanvasHeight()) stopThread = true;

			try {
				Thread.sleep(5);
			} catch (InterruptedException e) {
			}
		}
		// Remove ourselves from the list of particles
		synchronized (particles) {
			particles.remove(this);
		}
	}

	public void stopThread() {
		stopThread = true;
	}

	private void calculateAccel(RealPoint acceleration, Particle p) {
		double distanceFactor;
		if (power == Power.LINEAR) {
			distanceFactor = Point.distance(position.x, position.y, p.position.x, p.position.y);
		} else {
			distanceFactor = Point.distanceSq(position.x, position.y, p.position.x, p.position.y);
		}
		double chargeFactor = -1 * charge * p.charge * C;
		double force = chargeFactor / distanceFactor;
		double angle = Math.atan(Math.abs(p.position.y - position.y)
				/ Math.abs(p.position.x - position.x));
		double xAccel = force * Math.cos(angle) * Math.signum(p.position.x - position.x);
		double yAccel = force * Math.sin(angle) * Math.signum(p.position.y - position.y);
		acceleration.x += xAccel;
		acceleration.y += yAccel;
	}

	public void paint(Graphics g) {
		if (charge > 0) {
			g.setColor(Color.BLUE);
		} else {
			g.setColor(Color.RED);
		}
		g.fillOval(screenPosition.x - 4, screenPosition.y - 4, 9, 9);
		g.setColor(Color.BLACK);
		g.drawString(Integer.toString(charge), screenPosition.x - 3, screenPosition.y - 3);
	}
}
