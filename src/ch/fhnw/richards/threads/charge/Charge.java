package ch.fhnw.richards.threads.charge;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

public class Charge extends JFrame implements Runnable {
	JRadioButton btnLinear, btnQuadratic;
	JComboBox numParticles;
	MyCanvas canvas;
	JButton btnGo;
	ArrayList<Particle> particles = new ArrayList<Particle>();

	public static void main(String[] args) {
		new Charge();
	}

	public Charge() {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setLayout(new BorderLayout());

		Box controls = Box.createHorizontalBox();
		this.add(controls, BorderLayout.NORTH);

		canvas = new MyCanvas(particles);
		canvas.setPreferredSize(new Dimension(350, 350));
		this.add(canvas, BorderLayout.CENTER);

		ButtonGroup bg = new ButtonGroup();
		btnLinear = new JRadioButton("Linear");
		btnQuadratic = new JRadioButton("Quadratic");
		bg.add(btnLinear);
		bg.add(btnQuadratic);
		btnLinear.setSelected(true);
		controls.add(btnLinear);
		controls.add(btnQuadratic);

		controls.add(Box.createHorizontalGlue());

		controls.add(new JLabel("Number of particles"));

		numParticles = new JComboBox();
		numParticles.addItem(new Integer(2));
		numParticles.addItem(new Integer(3));
		numParticles.addItem(new Integer(4));
		numParticles.addItem(new Integer(5));
		numParticles.setSelectedIndex(1);
		controls.add(numParticles);
		controls.add(Box.createHorizontalGlue());

		btnGo = new JButton("Go");
		controls.add(btnGo);
		btnGo.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent evt) {
				startSim();
			}
		});

		pack();
		setVisible(true);
	}

	// Set the options, create the particles and start the simulation
	private void startSim() {
		btnGo.setEnabled(false);

		Random rand = new Random();
		if (btnLinear.isSelected()) {
			Particle.power = Particle.Power.LINEAR;
		} else {
			Particle.power = Particle.Power.QUADRATIC;
		}

		int totalCharge = 0;
		int charge;
		for (int i = (Integer) numParticles.getSelectedItem(); i > 0; i--) {
			if (i > 1) {
				int maxCharge = 2 * i - totalCharge;
				if (maxCharge > 5)
					maxCharge = 5;
				int minCharge = -2 * i - totalCharge;
				if (minCharge < 5)
					minCharge = -5;
				charge = 0;
				while (charge == 0) {
					charge = rand.nextInt(1 + maxCharge - minCharge)
							+ minCharge;
				}
				totalCharge += charge;
			} else {
				charge = -totalCharge;
			}

			Point pos = new Point(rand.nextInt(canvas.getWidth() - 200) + 100,
					rand.nextInt(canvas.getHeight() - 200) + 100);
			Particle p = new Particle(pos, charge, particles, this, "Particle "
					+ i);
			particles.add(p);
		}

		// Start particle threads
		synchronized (particles) {
			for (Particle p : particles) {
				p.start();
			}
		}

		// Painting thread
		Thread thisThread = new Thread(this, "Painter");
		thisThread.start();
	}

	@Override
	// Repeat forever: paint 33 frames/second
	public void run() {
		while (true) {
			canvas.repaint();
			try {
				Thread.sleep(30);
			} catch (InterruptedException e) {
			}
		}
	}
	
	public int getCanvasWidth() {
		return canvas.getWidth();
	}
	public int getCanvasHeight() {
		return canvas.getHeight();
	}

	// Using javax.swing.JPanel is a lot easier than java.awt.Canvas
	private static class MyCanvas extends JPanel {
		private ArrayList<Particle> particles;

		MyCanvas(ArrayList<Particle> particles) {
			this.particles = particles;
		}

		@Override
		public void paint(Graphics g) {
			BufferedImage buff = new BufferedImage(this.getWidth(), this.getHeight(), BufferedImage.TYPE_INT_RGB);
		    Graphics gBuff = buff.createGraphics();
		    
		    gBuff.setColor(Color.WHITE);
		    gBuff.fillRect(0, 0, this.getWidth(), this.getHeight());
			synchronized (particles) {
				for (Particle p : particles) {
					p.paint(gBuff);
				}
			}
			
		    g.drawImage(buff, 0, 0, buff.getWidth(), buff.getHeight(), this);
		}
	}
}
