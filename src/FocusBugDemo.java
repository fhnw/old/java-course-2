import java.awt.Component;
import java.awt.DefaultKeyboardFocusManager;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.KeyboardFocusManager;

import javax.swing.InputVerifier;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class FocusBugDemo extends JFrame {
	static JTextField txtOne = new JTextField("Ignore this control");
	static JTextField txtTwo = new JTextField("Delete this text, then press shift-tab");
	static JLabel lblFocus = new JLabel("");
	static KeyboardFocusManager kfm = new DefaultKeyboardFocusManager();

	public static void main(String[] args) {
		new FocusBugDemo();
		Thread t = new Thread() {
			@Override
			public void run() {
				while (true) {
					Component c = kfm.getFocusOwner();
					String focusInfo = "elsewhere";
					if (c == null) {
						focusInfo = "null";
					} else if (c == txtOne) {
						focusInfo = "txtOne";
					} else if (c == txtTwo) {
						focusInfo = "txtTwo";
					}
					lblFocus.setText(System.currentTimeMillis() + " - Focus owner " + focusInfo);
					try {
						Thread.sleep(1000);
					} catch (InterruptedException e) {
					}
				}
			}
		};
		t.start();
	}

	private FocusBugDemo() {
		super("Focus bug demo");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setPreferredSize(new Dimension(300, 100));
		setLayout(new GridLayout(3, 1));
		NotEmpty validator = new NotEmpty();
		txtOne.setInputVerifier(validator);
		txtTwo.setInputVerifier(validator);
		add(txtOne);
		add(txtTwo);
		add(lblFocus);
		pack();
		setVisible(true);
	}

	private class NotEmpty extends InputVerifier {
		@Override
		public boolean verify(JComponent input) {
			JTextField txtField = (JTextField) input;
			return (txtField.getText().length() > 0);
		}
	}
}
